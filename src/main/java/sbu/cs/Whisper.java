package sbu.cs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Whisper {
    // Variables:
    private String request;
    private Map<String, String> data;

    // Functions:
    public Whisper(String request, Map<String, String> data)
    {
        this.request = request;
        this.data = data;
    }

    public Whisper()
    {
        this.request = "";
        this.data = new HashMap<>();
    }

    public String getRequest()
    {
        return this.request;
    }

    public Map<String, String> getData()
    {
        return this.data;
    }

    public void setRequest(String request)
    {
        this.request = request;
    }

    public void setData(Map<String, String> data)
    {
        this.data = data;
    }

    public Set<String> getKeys()
    {
         return this.data.keySet();
    }

    public Collection<String> getValues() {
        return this.data.values();
    }

    public String getValueWithKey(String key)
    {
        return this.data.get(key);
    }

    public void addData(String key, String value)
    {
        this.data.put(key, value);
    }

    public void addStatusCode(String statusCode)
    {
        this.data.put("Status Code", statusCode);
    }

    public String toString()
    {
        String answer = "";
        answer += "Request: " + this.request;
        answer += '\n';
        answer += "Data: \n";
        Set<String> keyset = this.data.keySet();
        for (String key : keyset)
        {
            answer += '\t';
            answer += "key: " + key + '\n';
            answer += '\t';
            answer += "value: " + this.data.get(key) + '\n';
        }
        return answer;
    }

    public void createError(String errorStatus, String errorCode)
    {
        this.request = "Error";
        this.data.put("Error Status", errorStatus);
        this.data.put("Error Code", errorCode);
    }

    public void createUserObject(sbu.cs.classes.UserObject user)
    {
        this.request = "UserObject";
        this.data.put("ID", String.valueOf(user.getID()));
        this.data.put("username", user.getUsername());
        this.data.put("password", user.getPassword());
        this.data.put("bio", user.getBio());
        this.data.put("isPrivate", String.valueOf(user.isPrivate()));
        this.data.put("profilePictureAddress", user.getProfilePictureAddress());
        this.data.put("email", user.getEmail());
        this.data.put("sex", String.valueOf(user.getSex()));
    }

    public void createSuccessfulTask()
    {
        this.request = "Task Done";
        this.data.put("Status Code", "200");
    }

    public void createEndTask()
    {
        this.request = "End";
        this.data = null;
    }

    public void createNewUserTask(String username, String password, String email)
    {
        this.request = "Sign Up";
        this.data.put("username", username);
        this.data.put("password", password);
        this.data.put("email", email);
    }

    public void createCommentObject(int ID, int userID, int postID, String text) // For service to client
    {
        request = "Comment Data";
        data.put("ID", Integer.toString(ID));
        data.put("userID", Integer.toString(userID));
        data.put("postID", Integer.toString(postID));
        data.put("commentText", text);
    }

    public void createCommentObject(int userID, int postID, String text) // For client to service
    {
        request = "Comment Data";
        data.put("userID", Integer.toString(userID));
        data.put("postID", Integer.toString(postID));
        data.put("commentText", text);
    }

    public void createPostObject(int ID, int userID, String pictureAddress, String caption, String postDate) // Service to client post data.
    {
        request = "Post Data";
        data.put("ID", Integer.toString(ID));
        data.put("userID", Integer.toString(userID));
        data.put("postID", pictureAddress);
        data.put("pictureAddress", pictureAddress);
        data.put("postDate", postDate);
    }

    public void createPostObject(int userID, String caption, String postDate) // Client to server post data.
    {
        request = "Post Data";
        data.put("userID", Integer.toString(userID));
        data.put("caption", caption);
        data.put("postDate", postDate);
    }

    public void createPostObject(String username, String caption ,  String fileName , int fileSize)
    {
        request = "Add Post";
        data.put("username", username);
        data.put("caption", caption);
    }

    public void createNotFoundRecord()
    {
        request = "No Record Found";
        data = null;
    }

    public void createPictureDetails(String fileName, int fileSize)
    {
        request = "Picture Details";
        data.put("fileName", fileName);
        data.put("fileSize", Integer.toString(fileSize));
    }

}
