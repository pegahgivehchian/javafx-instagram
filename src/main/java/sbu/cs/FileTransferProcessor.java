package sbu.cs;

import java.io.*;
import java.net.Socket;

public class FileTransferProcessor {
    // Variables:
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private int fileSize;
    private String fileAddress;
    private File file;

    // Functions:
    public FileTransferProcessor(Socket socket) throws Exception
    {
        this.socket = socket;
    }

    public void fileReceiver(String fileAddress, int fileSize) throws Exception
    {
        dataInputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.fileAddress = fileAddress;
        this.fileSize = fileSize;
        System.out.println("File name: " + fileAddress);
        System.out.println("File size: " + fileSize);
        byte[] fileContent = new byte[fileSize];
        dataInputStream.readFully(fileContent);
        File fileObj = new File(fileAddress);
        FileOutputStream fileOutputStream = new FileOutputStream(fileObj);
        fileOutputStream.write(fileContent);
        String validation = "";
        if (fileSize == (int)fileObj.length())
        {
            validation = "Done";
            dataOutputStream.writeUTF(validation);
        }
        else
        {
            while (fileSize != (int)fileObj.length())
            {
                validation = "Not Done";
                dataOutputStream.writeUTF(validation);
                dataInputStream.readFully(fileContent);
                fileOutputStream.write(fileContent);
            }
            validation = "Done";
            dataOutputStream.writeUTF(validation);
        }

        fileOutputStream.close();
        return;
    }

    public void fileSender(File file) throws Exception
    {
        dataInputStream = new DataInputStream(socket.getInputStream());
        dataOutputStream = new DataOutputStream(socket.getOutputStream());
        byte[] fileContent = fileToByte(file);
        dataOutputStream.write(fileContent);
        String validation = "";
        validation = dataInputStream.readUTF();
        while (!validation.equals("Done"))
        {
            dataOutputStream.write(fileContent);
            validation = dataInputStream.readUTF();
        }
        return;
    }

    private byte[] fileToByte(File f) throws IOException
    {
        FileInputStream fin = null;
        fin = new FileInputStream(f);

        byte fileContent[] = new byte[(int)f.length()];
        fin.read(fileContent);
        fin.close();
        return fileContent;
    }

}