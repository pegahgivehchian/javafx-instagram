package sbu.cs.server;

import com.google.gson.GsonBuilder;
import sbu.cs.FileTransferProcessor;
import sbu.cs.Translator;
import sbu.cs.Whisper;
import sbu.cs.classes.CommentObject;
import sbu.cs.classes.PostObject;
import sbu.cs.classes.UserObject;
//import sbu.cs.database.SQLConnection;
import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Service implements Runnable{
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    //private SQLConnection sqlConnection;
    private UserObject userObjectMain;
    private Translator translator;
    private FileTransferProcessor fileTransferProcessor;

//    public Service(Socket socket, SQLConnection sqlConnection) throws Exception
//    {
//        this.socket = socket;
//        this.sqlConnection = sqlConnection;
//        this.dataInputStream = new DataInputStream(this.socket.getInputStream());
//        this.dataOutputStream = new DataOutputStream(this.socket.getOutputStream());
//        userObjectMain = null;
//        translator = new Translator();
//        fileTransferProcessor = new FileTransferProcessor(socket);
//    }

    public Service(Socket socket) throws Exception
    {
        this.socket = socket;
        //this.sqlConnection = sqlConnection;
        this.dataInputStream = new DataInputStream(this.socket.getInputStream());
        this.dataOutputStream = new DataOutputStream(this.socket.getOutputStream());
        userObjectMain = null;
        translator = new Translator();
        fileTransferProcessor = new FileTransferProcessor(socket);
    }

    public void signUpFunction(Whisper input) {
        String username = input.getValueWithKey("username");
        String password = input.getValueWithKey("password");
        String email = input.getValueWithKey("email");
//        try
//        {
//            boolean isEmailUsed = sqlConnection.isEmailUsed(email);
//            boolean isUsernameAvailable = sqlConnection.isUsernameAvailable(username);
//            boolean isUserSignedUp = sqlConnection.isUserSignedUp(username, password, email);
//            if (isUserSignedUp)
//            {
//                Whisper response = new Whisper();
//                response.createError("User signed up before", "201");
//                String output = translator.whisperToString(response);
//                try
//                {
//                    dataOutputStream.writeUTF(output);
//                    dataOutputStream.flush();
//                    return;
//                }
//                catch (IOException exception)
//                {
//                    exception.printStackTrace();
//                }
//            }
//
//            else if (isEmailUsed)
//            {
//                Whisper response = new Whisper();
//                response.createError("Email used before", "202");
//                String output = translator.whisperToString(response);
//                try
//                {
//                    dataOutputStream.writeUTF(output);
//                    dataOutputStream.flush();
//                    return;
//                }
//                catch (IOException exception)
//                {
//                    exception.printStackTrace();
//                }
//            }
//
//            else if (!isUsernameAvailable)
//            {
//                Whisper response = new Whisper();
//                response.createError("Username has been taken", "203");
//                String output = translator.whisperToString(response);
//                try
//                {
//                    dataOutputStream.writeUTF(output);
//                    dataOutputStream.flush();
//                    return;
//                }
//                catch (IOException exception)
//                {
//                    exception.printStackTrace();
//                }
//            }
//
//            else
//            {
//                boolean isDone = sqlConnection.createNewUser(username, password, email);
//                if (isDone)
//                {
        Whisper response = new Whisper();
        response.createSuccessfulTask();
        System.out.println("created repository : " + createNewDirectory(username));
        String output = translator.whisperToString(response);
        try {
            dataOutputStream.writeUTF(output);
            dataOutputStream.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        finally {
            return;
        }
//                }
//                else
//                {
//                    Whisper response = new Whisper();
//                    response.createError("Problem occurred in database", "500");
//                    String output = translator.whisperToString(response);
//                    try
//                    {
//                        dataOutputStream.writeUTF(output);
//                        dataOutputStream.flush();
//                    }
//                    catch (IOException exception)
//                    {
//                        exception.printStackTrace();
//                    }
//                    finally {
//                        return;
//                    }
//                }
//            }
//        }
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
//    }
    }

    public void signInFunction(Whisper input)
    {
        String username = input.getValueWithKey("username");
        String password = input.getValueWithKey("password");
//        try
//        {
//            userObjectMain = sqlConnection.signIn(username, password);
//        }
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
//        if (userObjectMain == null)
//        {
//            Whisper output = new Whisper();
//            output.createError("Invalid username or password", "301");
//            String outputString = translator.whisperToString(output);
//            try {
//                dataOutputStream.writeUTF(outputString);
//                dataOutputStream.flush();
//            }
//            catch (IOException exception)
//            {
//                exception.printStackTrace();
//            }
//            finally {
//                return;
//            }
//        }
//        else
//        {
        Whisper response = new Whisper();
        response.createSuccessfulTask();
        String output = translator.whisperToString(response);
        try {
            dataOutputStream.writeUTF(output);
            dataOutputStream.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        finally {
            return;
        }
//        }
    }

    public void followFunction(Whisper input)
    {
        String follower, followed;
        follower = input.getValueWithKey("follower");
        followed = input.getValueWithKey("followed");
 //       try {
//            boolean res = sqlConnection.addRelationShip(follower, followed);
//            if (res)
//            {
                //System.out.println("New relationShip added to DB!");
                Whisper outputWhisper = new Whisper();
                outputWhisper.createSuccessfulTask();
                String outputString = translator.whisperToString(outputWhisper);
                try {
                    dataOutputStream.writeUTF(outputString);
                    dataOutputStream.flush();
                }catch (IOException exception)
                {
                    exception.printStackTrace();
                }
 //           }
//            else {
//                System.out.println("Some Error occurred!");
//                Whisper outputWhiper = new Whisper();
//                outputWhiper.createError("Follow Failed", "601");
//                String outputString = translator.whisperToString(outputWhiper);
//                try
//                {
//                    dataOutputStream.writeUTF(outputString);
//                    dataOutputStream.flush();
//                }
//                catch (IOException exception)
//                {
//                    exception.printStackTrace();
//                }
//            }
//        }catch (SQLException sqlException){
//            sqlException.printStackTrace();
//        }


    }

    public void unfollowFunction(Whisper input)
    {
        String follower, followed;
        follower = input.getValueWithKey("follower");
        followed = input.getValueWithKey("followed");
        Whisper outputWhisper = new Whisper();
//        try
//        {
//            if (sqlConnection.isFollowed(followerID, followedID))
//            {
//                boolean res;
//                res = sqlConnection.deleteRelationShip(followerID, followedID);
//                if (res)
//                {
                    outputWhisper.createSuccessfulTask();
                    String outputString = translator.whisperToString(outputWhisper);
                    try
                    {
                        dataOutputStream.writeUTF(outputString);
                        dataOutputStream.flush();
                    }catch (IOException exception)
                    {
                        exception.printStackTrace();
                    }
//                }
//                else
//                {
//                    outputWhisper.createError("DataBase problem","500");
//                    String outputString = translator.whisperToString(outputWhisper);
//                    try
//                    {
//                        dataOutputStream.writeUTF(outputString);
//                        dataOutputStream.flush();
//                    }
//                    catch (IOException exception)
//                    {
//                        exception.printStackTrace();
//                    }
//                }
//
//            }
//            else
//            {
//                outputWhisper.createError("Not Followed", "602");
//                String outputString = translator.whisperToString(outputWhisper);
//                try
//                {
//                    dataOutputStream.writeUTF(outputString);
//                    dataOutputStream.flush();
//                }
//                catch (IOException exception)
//                {
//                    exception.printStackTrace();
//                }
//            }
//
//        }
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
    }

    public void addPost(Whisper input) // Adding new post version 1
    {
        String username = input.getValueWithKey("username");
        String caption = input.getValueWithKey("caption");
        String fileName = input.getValueWithKey("fileName");
        String fileSize = input.getValueWithKey("fileSize");
//        try
//        {
            String pictureAddress = "./Resources/" + username + '/' + fileName;
//            boolean sit = sqlConnection.addNewPost(username, caption, pictureAddress);
//            if (sit)
//            {
                Whisper outputWhisper = new Whisper();
                outputWhisper.createSuccessfulTask();
                String output = translator.whisperToString(outputWhisper);
                try
                {
                    dataOutputStream.writeUTF(output);
                    dataOutputStream.flush();
                }
                catch (IOException conException)
                {
                    conException.printStackTrace();
                }
                try
                {
                    fileTransferProcessor.fileReceiver(pictureAddress, Integer.valueOf(fileSize));
                    outputWhisper.createSuccessfulTask();
                    output = translator.whisperToString(outputWhisper);
                    dataOutputStream.writeUTF(output);
                    dataOutputStream.flush();
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    outputWhisper.createError("Couldn't download picture", "702");
                    output = translator.whisperToString(outputWhisper);
                    try
                    {
                        dataOutputStream.writeUTF(output);
                        dataOutputStream.flush();
                    }
                    catch (Exception exception1)
                    {
                        exception1.printStackTrace();
                    }
                }
           // }

//            else
//            {
//                Whisper outputWhisper = new Whisper();
//                outputWhisper.createError("Couldn't add new Post!", "701");
//                String output = translator.whisperToString(outputWhisper);
//                try
//                {
//                    dataOutputStream.writeUTF(output);
//                    dataOutputStream.flush();
//                }
//                catch (IOException exception)
//                {
//                    exception.printStackTrace();
//                }
//            }
//        }
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
    }

//    public void addLike(Whisper input)
//    {
//        try
//        {
//            int userID = sqlConnection.getUserID(input.getValueWithKey("username"));
//            int postID = Integer.valueOf(input.getValueWithKey("postID"));
//            boolean answer = sqlConnection.addNewLike(userID, postID);
//            if (answer)
//            {
//                Whisper outputWhisper = new Whisper();
//                outputWhisper.createSuccessfulTask();
//                String output = translator.whisperToString(outputWhisper);
//                try
//                {
//                    dataOutputStream.writeUTF(output);
//                    dataOutputStream.flush();
//                }
//                catch (Exception ex)
//                {
//                    ex.printStackTrace();
//                }
//            }
//            else
//            {
//                Whisper outputWhisper = new Whisper();
//                outputWhisper.createError("Couldn't like this post","801");
//                String output = translator.whisperToString(outputWhisper);
//                try
//                {
//                    dataOutputStream.writeUTF(output);
//                    dataOutputStream.flush();
//                }
//                catch (Exception ex)
//                {
//                    ex.printStackTrace();
//                }
//            }
//        }
//        catch (Exception exception)
//        {
//            exception.printStackTrace();
//        }
//    }
//
//    public void addComment(Whisper input) throws SQLException
//    {
//        int userID = Integer.valueOf(input.getValueWithKey("userID"));
//        int postID = Integer.valueOf(input.getValueWithKey("postID"));
//        String text = input.getValueWithKey("commentText");
//        boolean isTaskDone = sqlConnection.addNewComment(userID, postID, text);
//        if (isTaskDone)
//        {
//            Whisper outputWhisper = new Whisper();
//            outputWhisper.createSuccessfulTask();
//            String output = translator.whisperToString(outputWhisper);
//            try
//            {
//                dataOutputStream.writeUTF(output);
//                dataOutputStream.flush();
//                return;
//            }
//            catch (IOException exception)
//            {
//                exception.printStackTrace();
//            }
//        }
//        else {
//            Whisper outputWhisper = new Whisper();
//            outputWhisper.createError("Can not add new comment", "901");
//            String output = translator.whisperToString(outputWhisper);
//            try
//            {
//                dataOutputStream.writeUTF(output);
//                dataOutputStream.flush();
//            }
//            catch (IOException exception)
//            {
//                exception.printStackTrace();
//            }
//            return;
//        }
//    }

    public void retrievingPosts(Whisper input) throws IOException {
        String username = input.getValueWithKey("username");
        ArrayList<PostObject> posts = new ArrayList<>();
        //posts.add(new PostObject(username, "./Resources/" + username + "/pic.jpg", "hi"));
        PostObject post1 = new PostObject(username, "./Resources/" + username + "/pic7.png", "nfsrgsg");
        ArrayList<String> likers = new ArrayList<>();
        likers.add("mobin");
        likers.add("farimah");
        post1.setLikers(likers);
        ArrayList<CommentObject> comments = new ArrayList<>();
        CommentObject cm1 = new CommentObject("farimah" , "shululululu");
        CommentObject cm2 = new CommentObject("mobin" , "baribaba");
        comments.add(cm1);
        comments.add(cm2);
        post1.setComments(comments);
        posts.add(post1);
        PostObject post2 = (new PostObject(username, "./Resources/" + username + "/Screenshot(2).png", "fkbaekfa"));
        likers = new ArrayList<>();
        likers.add("aliabadi");
        likers.add("kobra");
        likers.add("soghra");
        post2.setLikers(likers);
        comments = new ArrayList<>();
        cm1 = new CommentObject("farimah" , "shululululu");
        cm2 = new CommentObject("pegah" , "COMMENT");
        CommentObject cm3 = new CommentObject("aliabadi" , "COMMENT2");
        comments.add(cm1);
        comments.add(cm2);
        comments.add(cm3);
        post2.setComments(comments);
        posts.add(post2);
        PostObject post3 = (new PostObject(username, "./Resources/" + username + "/Screenshot(7).png", "shuulululu"));
        likers = new ArrayList<>();
        likers.add("aliabadi");
        likers.add("kobra");
        likers.add("soghra");
        post3.setLikers(likers);
        posts.add(post3);
        //        try
//        {
//            ArrayList<PostObject> posts = sqlConnection.getAllPosts(username);
        if (posts.size() == 0) {
            Whisper outputWhisper = new Whisper();
            outputWhisper.createNotFoundRecord();
            String output = translator.whisperToString(outputWhisper);
            try {
                dataOutputStream.writeUTF(output);
                dataOutputStream.flush();
                return;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else {
            Whisper outputWhisper = new Whisper();
            outputWhisper.createSuccessfulTask();
            String output = translator.whisperToString(outputWhisper);
            try {
                dataOutputStream.writeUTF(output);
                dataOutputStream.flush();
                output = translator.postsToString(posts);
                dataOutputStream.writeUTF(output);
                dataOutputStream.flush();
                for (PostObject post : posts) {
                    String pictureAddress = post.getPictureAddress();
                    File file = new File(pictureAddress);
                    outputWhisper = new Whisper();
                    outputWhisper.createPictureDetails(file.getName(), (int) file.length());
                    output = translator.whisperToString(outputWhisper);
                    try {
                        dataOutputStream.writeUTF(output);
                        dataOutputStream.flush();
                        fileTransferProcessor.fileSender(file);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }

        }
    //}
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
    }

    public void getChattedUsers(Whisper input)
    {
        String username = input.getValueWithKey("username");
        ArrayList<String> users = new ArrayList<>();
//        try
//        {
            users.add("pegah");
            users.add("mobin");
            //users = sqlConnection.getChattedUsers(username);
            String output = translator.stringArrayListToString(users);
            try
            {
                dataOutputStream.writeUTF(output);
                dataOutputStream.flush();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

       // }
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
    }

    public void usernameChecker(Whisper input)
    {
        String username = input.getValueWithKey("username");
//        try
//        {
            //boolean isAvailable = !sqlConnection.isUsernameAvailable(username);
            boolean isAvailable = false;
            System.out.println("searched = " + username);
            if ((username.equals("mobin"))||username.equals("pegah")||username.equals("farimah")){
                isAvailable=true;
            }
            Whisper outputWhisper = new Whisper();
            outputWhisper.setRequest("Request Response");
            outputWhisper.addData("isAvailable", String.valueOf(isAvailable));
            String output = translator.whisperToString(outputWhisper);
            try
            {
                dataOutputStream.writeUTF(output);
                dataOutputStream.flush();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }

        //}
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//            try
//            {
//                Whisper outputWhisper = new Whisper();
//                outputWhisper.createError("Database Error", "500");
//                String output = translator.whisperToString(outputWhisper);
//                dataOutputStream.writeUTF(output);
//                dataOutputStream.flush();
//            }
//            catch (Exception exception1)
//            {
//                exception1.printStackTrace();
//            }
//        }
    }

    public void followStateChecker(Whisper input){

        String id1 = input.getValueWithKey("follower");
        String id2 = input.getValueWithKey("followed");
        //boolean answer = sqlConnection.isFollowed(id1,id2);
        boolean answer = false;
        Whisper outputWhisper = new Whisper();
        outputWhisper.setRequest("Return Follow State");
        outputWhisper.addData("isFollowed", String.valueOf(answer));
        String output = translator.whisperToString(outputWhisper);
        try
        {
            dataOutputStream.writeUTF(output);
            dataOutputStream.flush();
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void profileInfo(Whisper input) throws IOException {
        String username = input.getValueWithKey("username");
        ArrayList<String> followers = new ArrayList<>();
        followers.add("mahla");
        followers.add("shahla");
        ArrayList<String> followings = new ArrayList<>();
        followings.add("soghra");
        followings.add("kobra");
//        try
//        {
//            String profilePictureAddress = sqlConnection.getProfilePictureAddress(username);
//            String bio = sqlConnection.getBio(username);
//            File file = new File(profilePictureAddress);
            File file = new File("./ProfilePictures/profile1.jpg");
            String fileName = file.getName();
            int fileSize = (int)file.length();
            String bio = "BIO";
//            followers = sqlConnection.getFollowers(username);
//            followings = sqlConnection.getFollowings(username);
            Whisper outputWhisper = new Whisper();
            outputWhisper.setRequest("Profile Info");
            outputWhisper.addData("username", username);
            outputWhisper.addData("followers", translator.stringArrayListToString(followers));
            outputWhisper.addData("followings", translator.stringArrayListToString(followings));
            outputWhisper.addData("fileName", fileName);
            outputWhisper.addData("fileSize", Integer.toString(fileSize));
            outputWhisper.addData("bio", bio);
            dataOutputStream.writeUTF(translator.whisperToString(outputWhisper));
            dataOutputStream.flush();

            if (input.getValueWithKey("noProfilePic").equals("false")) {
                try {
                    fileTransferProcessor.fileSender(file);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        //}
//        catch (SQLException exception)
//        {
//            exception.printStackTrace();
//        }
    }

    public void setProfilePicture(Whisper input)
    {
        String username = input.getValueWithKey("username");
        String fileName = input.getValueWithKey("fileName");
        int fileSize = Integer.valueOf(input.getValueWithKey("fileSize"));
        try
        {
            fileTransferProcessor.fileReceiver("./ProfilePictures/" + username + "/" + fileName, fileSize);
            //boolean isJobDone = sqlConnection.updateProfilePictureAddress("./ProfilePictures/" + username + "/" + fileName, username);
//            if (isJobDone)
//            {
                Whisper outputWhisper = new Whisper();
                outputWhisper.createSuccessfulTask();
                try
                {
                    dataOutputStream.writeUTF(translator.whisperToString(outputWhisper));
                    dataOutputStream.flush();
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            //}
            //else {
//                Whisper outputWhisper = new Whisper();
//                outputWhisper.createError("Database Error", "500");
//                try {
//                    dataOutputStream.writeUTF(translator.whisperToString(outputWhisper));
//                    dataOutputStream.flush();
//                }
//                catch (Exception exception)
//                {
//                    exception.printStackTrace();
//                }
           //}
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public boolean createNewDirectory (String name)
        {
        String path = "./Resources/";
        path += name;
        File file = new File(path);
        boolean isDone = file.mkdir();
        return isDone;
    }

    @Override
    public void run(){
        System.out.println(Thread.currentThread());
        try
        {
            while (true) {
                System.out.println("Waiting for request...");
                String socketInput = dataInputStream.readUTF();
                System.out.println("Got new request!");
                Whisper inputWhisper = translator.stringToWhisper(socketInput);
                String request = inputWhisper.getRequest();
                if (request.equals("Sign Up")) {
                    // SignUp function
                    signUpFunction(inputWhisper);
                }

                else if (request.equals("Sign In")) {
                    // SignIn function
                    signInFunction(inputWhisper);
                }

                else if (request.equals("Add Post")) {
                    // Upload Image function
                    addPost(inputWhisper);
                }


                else if (request.equals("Follow Request"))
                {
                    // Follow Request function
                    followFunction(inputWhisper);
                }

                else if (request.equals("Unfollow Request"))
                {
                     //Unfollow Request function
                    unfollowFunction(inputWhisper);
                }

                else if (request.equals("Retrieving Posts"))
                {
                    // Sending Posts from server to client.
                    retrievingPosts(inputWhisper);
                }

                else if (request.equals("Chatted Users"))
                {
                    // Getting all users which chatted with specific user.
                }

                else if (request.equals("Check Username availability"))
                {
                    // Check if a username is available or no.
                    usernameChecker(inputWhisper);
                }

                else if (request.equals("Get Follow State")){
                    followStateChecker(inputWhisper);
                }

                else if (request.equals("Profile Info"))
                {
                    // Send profile info
                    profileInfo(inputWhisper);
                }

                else if (request.equals("Set Profile Picture"))
                {
                    // Set new profile picture for user.
                    //setProfilePicture(inputWhisper);
                }

                else if (request.equals("Add DirectMessage"))
                {
                    // Add new direct message.
                }

                else if (request.equals("End")) {
                    // End of communication!
                    System.out.println("End of this service!");
                    socket.close();
                    return;
                }

                else {
                    Whisper outputWhisper = new Whisper();
                    outputWhisper.createError("Invalid Command","404");
                    String packet = translator.whisperToString(outputWhisper);
                    dataOutputStream.writeUTF(packet);
                    dataOutputStream.flush();
                }
            }
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
            return;
        }
    }
}
