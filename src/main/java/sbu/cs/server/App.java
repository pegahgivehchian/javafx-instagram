package sbu.cs.server;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    // Variables:
    private static final int THREADNUMBER = 4;
    private static ExecutorService executorService = Executors.newFixedThreadPool(THREADNUMBER);


    // Functions:

    public static void main(String[] args) throws SQLException, ClassNotFoundException, Exception {
        try {
            ServerSocket serverSocket = new ServerSocket(2727);
            //SQLConnection sqlConnection = new SQLConnection();
            while (true)
            {
                Socket socket = serverSocket.accept();
                System.out.println("One Client connected!");
                //Service service = new Service(socket, sqlConnection);
                Service service = new Service(socket);
                new Thread(service).start();
            }


        }
        catch (IOException exception)
        {
            System.out.println("Can not run server!");
            exception.printStackTrace();
        }
    }
}
