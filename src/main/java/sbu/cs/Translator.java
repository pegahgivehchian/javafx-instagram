package sbu.cs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import javafx.geometry.Pos;
import sbu.cs.classes.PostObject;

import java.util.ArrayList;
import java.util.List;

public class Translator {
    // Variables:
    private  GsonBuilder gsonBuilder = new GsonBuilder() ;
    private Gson gson = gsonBuilder.create();

    public Translator() {
        gsonBuilder = new GsonBuilder() ;
        gson = gsonBuilder.create();
    }

    // Functions:

    public String whisperToString(Whisper input)
    {
        String output;
        output = gson.toJson(input);
        return output;
    }

    public Whisper stringToWhisper(String input)
    {
        Whisper output = new Whisper();
        output = gson.fromJson(input, Whisper.class);
        return output;
    }

    public Whisper stringToWhisperArray(String input)
    {
        Whisper output = new Whisper();
        output = gson.fromJson(input, Whisper.class);
        return output;
    }

    public String postsToString(ArrayList<PostObject> posts)
    {
        String output;
        output = gson.toJson(posts);
        return output;
    }

    public ArrayList<PostObject> stringToPosts(String input)
    {
        System.out.println("input :" + input);
        ArrayList<PostObject> posts;
        posts = gson.fromJson(input, new TypeToken<List<PostObject>>(){}.getType());
        return posts;
    }

    public String stringArrayListToString(ArrayList<String> input)
    {
        String answer = gson.toJson(input);
        return answer;
    }

    public ArrayList<String> stringToStringArrayList(String input)
    {
        ArrayList<String> answer = gson.fromJson(input, new TypeToken<List<String>>(){}.getType());
        return answer;
    }
}
