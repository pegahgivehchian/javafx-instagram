package sbu.cs.databases;

import sbu.cs.classes.CommentObject;
import sbu.cs.classes.DirectMessageObject;
import sbu.cs.classes.PostObject;
import sbu.cs.classes.UserObject;

import javax.xml.stream.events.Comment;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;


public class SQLConnection implements Closeable {
    // Variables:
    private final String CONN_URL = "jdbc:mysql://localhost:3306/Instagram?autoReconnect=true&useSSL=false";
    private final String DBUsername = "root";
    private final String DBPassword = "12345678";
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet results;
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    // Functions:
    public SQLConnection() throws SQLException, ClassNotFoundException
    {
        connection = DriverManager.getConnection(CONN_URL, DBUsername, DBPassword);
        statement = connection.createStatement();
    }

    public ResultSet getResults()
    {
        return this.results;
    }

    public int getUserID(String username) throws SQLException // Getting userID from a username
    {
        String query = "select * from Users where username = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        results = preparedStatement.executeQuery();
        if (results.next()) {
            return results.getInt("ID");
        }
        else
        {
            return -1;
        }
    }

    public String getUserName(int userID) throws  SQLException // Getting username from a userID
    {
        String query = "select username from Users where ID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1,userID);
        results = preparedStatement.executeQuery();
        return results.getString("username");
    }

    public boolean createNewUser(String username, String password, String email)
    {
        try
        {
            String query = "insert into Users (username, password, email) values (?, ?, ?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, email);
            int i = 0;
            i = preparedStatement.executeUpdate();
            System.out.printf("%d rows effected \n", i);
            preparedStatement.close();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException exception)
        {
            exception.printStackTrace();
            return false;
        }
    }

    public boolean isUsernameAvailable(String username) throws SQLException // Check if username is unique or not
    {
        boolean answer = false;
        String query = "select * from Users where username=?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        results = preparedStatement.executeQuery();
        answer = results.next();
        return !answer;
    }

    public boolean isUserSignedUp(String username, String password, String email) throws SQLException // Check if a user signed up before or not
    {
        boolean answer = false;
        String query = "select * from Users where username=? and password=? and email=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);
        preparedStatement.setString(3, email);
        results = preparedStatement.executeQuery();
        answer = results.next();
        return answer;
    }

    public boolean isEmailUsed(String email) throws SQLException
    {
        boolean answer;
        String query = "select * from Users where email=?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, email);
        results = preparedStatement.executeQuery();
        answer = results.next();
        return answer;
    }

    public UserObject resultSetToUserObject() throws SQLException  // This function transfer data from result set into UserObject
    {
        int ID = results.getInt("ID");
        String password = results.getString("password");
        String username = results.getString("username");
        String bio = results.getString("bio");
        boolean isPrivate = results.getBoolean("isPrivate");
        String profilePictureAddress = results.getString("profilePictureAddress");
        String email = results.getString("email");
        int sexNumber = results.getInt("sex");
        UserObject.Sex sex = null;
        if (sexNumber == 0)
        {
            sex = UserObject.Sex.MALE;
        }
        else if (sexNumber == 1)
        {
            sex = UserObject.Sex.FEMALE;
        }
        else if (sexNumber == 2)
        {
            sex = UserObject.Sex.NOTBINARY;
        }
        else
        {
            sex = null;
        }
        UserObject user = new UserObject(ID, username, password, bio, isPrivate, profilePictureAddress, email, sex);
        return user;
    }

    public UserObject signIn(String username, String password) throws SQLException
    {
        String query = "select * from Users where username=? and password=?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);
        results = preparedStatement.executeQuery();
        boolean answer = results.next();
        if (!answer)
        {
            return null;
        }
        else
        {
            return resultSetToUserObject();
        }

    }

    public boolean addRelationShip(int followerID, int followedID) throws SQLException // Create new RelationShip in RelationShip server
    {
        boolean isOK = !isFollowed(followerID, followedID);
        if (isOK) {
            String query = "Insert into RelationShip (followerID, followedID) values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, followerID);
            preparedStatement.setInt(2, followedID);
            int i = preparedStatement.executeUpdate();
            if (i > 0) {
                return true;
            } else {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public boolean addRelationShip(String followerUsername, String followedUsername) throws SQLException
    {
        int followerID = getUserID(followerUsername);
        int followedID = getUserID(followedUsername);
        boolean isOK = !isFollowed(followerID, followedID);
        if (isOK) {
            String query = "Insert into RelationShip (followerID, followedID) values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, followerID);
            preparedStatement.setInt(2, followedID);
            int i = preparedStatement.executeUpdate();
            if (i > 0) {
                return true;
            } else {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public boolean deleteRelationShip(int followerID, int followedID) throws SQLException
    {

        String query = "Delete from RelationShip where followerID = ? and followedID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, followerID);
        preparedStatement.setInt(2, followedID);
        int i = preparedStatement.executeUpdate();
        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean deleteRelationShip(String followerUsername, String followedUsername) throws SQLException
    {
        int followerID = getUserID(followerUsername);
        int followedID = getUserID(followedUsername);
        String query = "Delete from RelationShip where followerID = ? and followedID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, followerID);
        preparedStatement.setInt(2, followedID);
        int i = preparedStatement.executeUpdate();
        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isFollowed(int followerID, int followedID) throws SQLException // isFollowed with userIDs
    {
        String query = "Select * from RelationShip where followerID = ? and followedID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, followerID);
        preparedStatement.setInt(2, followedID);
        results = preparedStatement.executeQuery();
        if (results.next())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isFollowed(String followerUsername, String followedUsername) throws SQLException // isFollowed with usernames
    {
        int followerID, followedID;
        followerID = getUserID(followerUsername);
        followedID = getUserID(followedUsername);
        String query = "Select * from RelationShip where followerID = ? and followedID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, followerID);
        preparedStatement.setInt(2, followedID);
        results = preparedStatement.executeQuery();
        if (results.next())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean addNewPost(int userID, String caption) throws SQLException // Create new post with userID, caption and postDate
    {
        String date = dtf.format(LocalDateTime.now());
        String query = "insert into Posts (userID, caption, postDate) values (?,?,?);";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1,userID);
        preparedStatement.setString(2,caption);
        preparedStatement.setString(3,date);
        int i = preparedStatement.executeUpdate();
        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean addNewPost(String username, String caption, String pictureAddress) throws SQLException
    {
        String date = dtf.format(LocalDateTime.now());
        String query = "insert into Posts (userID, caption, postDate, pictureAddress) values (?,?,?,?);";
        int userID = getUserID(username);
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1,userID);
        preparedStatement.setString(2, caption);
        preparedStatement.setString(3, date);
        preparedStatement.setString(4, pictureAddress);
        int i = preparedStatement.executeUpdate();
        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean addNewLike(int userID, int postID) throws SQLException
    {
        if (isUserIDAvailable(userID) && isPostAvailable(postID) && !isLikedBefore(userID, postID)) {
            String query = "insert into Likes (postID, userID) values (?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, postID);
            preparedStatement.setInt(2, userID);
            boolean answer = preparedStatement.execute();
            return answer;
        }
        else
        {
            return false;
        }
    }

    public boolean addNewLike(String username, int postID) throws SQLException
    {
        int userID = getUserID(username);
        if (isUserIDAvailable(userID) && isPostAvailable(postID) && !isLikedBefore(userID, postID)) {
            String query = "insert into Likes (postID, userID) values (?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, postID);
            preparedStatement.setInt(2, userID);
            int i = preparedStatement.executeUpdate();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public boolean deleteLike(int userID, int postID) throws SQLException
    {
        if (isUserIDAvailable(userID) && isPostAvailable(postID) && isLikedBefore(userID, postID))
        {
            String query = "delete from Likes where postID = ? and userID = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, postID);
            preparedStatement.setInt(2, userID);
            int i = preparedStatement.executeUpdate();
            if (i > 0)
            {
                return true;
            }
        }
        return false;
    }

    public boolean deleteLike(String username, int postID) throws SQLException
    {
        int userID = getUserID(username);
        if (isUserIDAvailable(userID) && isPostAvailable(postID) && isLikedBefore(userID, postID))
        {
            String query = "delete from Likes where postID = ? and userID = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, postID);
            preparedStatement.setInt(2, userID);
            int i = preparedStatement.executeUpdate();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public boolean isPostAvailable(int postID) throws SQLException
    {
        String query = "select * from Posts where ID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, postID);
        int i = preparedStatement.executeUpdate();
        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isUserIDAvailable(int userID) throws SQLException
    {
        String query = "select * from Users where ID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        int i = preparedStatement.executeUpdate();
        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isLikedBefore(int userID, int postID) throws SQLException
    {
        String query = "select * from Likes where postID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, postID);
        ResultSet res = preparedStatement.executeQuery();
        while (res.next())
        {
            if (res.getInt("userID") == userID )
            {
                return true;
            }
        }
        return false;
    }

    public boolean isLikedBefore(String username, int postID) throws SQLException
    {
        int userID = getUserID(username);
        String query = "select * from Likes where postID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, postID);
        ResultSet res = preparedStatement.executeQuery();
        while (res.next())
        {
            if (res.getInt("userID") == userID )
            {
                return true;
            }
        }
        return false;
    }

    public boolean isCommentedBefore(int userID, int postID, String text) throws SQLException // To seeing if some one commented before or not.
    {
        String query = "select * from Comments where userID = ? and postID = ? and commentText = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        preparedStatement.setInt(2, postID);
        preparedStatement.setString(3, text);
        ResultSet res = preparedStatement.executeQuery();
        if (res.next())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isCommentedBefore(String username, int postID, String text) throws SQLException
    {
        int userID = getUserID(username);
        String query = "select * from Comments where userID = ? and postID = ? and commentText = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        preparedStatement.setInt(2, postID);
        preparedStatement.setString(3, text);
        ResultSet res = preparedStatement.executeQuery();
        if (res.next())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean addNewComment(int userID, int postID, String text) throws SQLException
    {
        boolean isOk = isCommentedBefore(userID, postID, text);
        if (isOk)
        {
            return false;
        }
        else
        {
            String query = "insert into Comments(userID, postID, commentText) values (? , ? , ?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, postID);
            preparedStatement.setString(3, text);
            int i = preparedStatement.executeUpdate();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public boolean addNewComment(String username, int postID, String text) throws SQLException
    {
        int userID = getUserID(username);
        boolean isOk = isCommentedBefore(userID, postID, text);
        if (isOk)
        {
            return false;
        }
        else
        {
            String query = "insert into Comments(userID, postID, commentText) values (? , ? , ?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, postID);
            preparedStatement.setString(3, text);
            int i = preparedStatement.executeUpdate();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public ArrayList<Integer> getAllPostsID(String username) throws SQLException // Getting a user's posts ID.
    {
        ArrayList<Integer> answer = new ArrayList<>();
        int userID = getUserID(username);
        String query = "select * from Posts where userID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        ResultSet res = preparedStatement.executeQuery();
        while (res.next())
        {
            answer.add(res.getInt("ID"));
        }
        return answer;
    }

    public ArrayList<PostObject> getAllPosts(String username) throws SQLException
    {
        ArrayList<PostObject> answer = new ArrayList<>();
        ArrayList<Integer> postIDs = new ArrayList<>();
        postIDs = getAllPostsID(username);
        String query = "select * from Posts where ID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet res;
        for (int i : postIDs)
        {
            preparedStatement.setInt(1, i);
            res = preparedStatement.executeQuery();
            PostObject postObject = resultsToPost(res);
            answer.add(postObject);
        }
        return answer;
    }

    private PostObject resultsToPost(ResultSet res) throws SQLException
    {
        int userID = res.getInt("userID");
        String pictureAddress = res.getString("pictureAddress");
        String caption = res.getString("caption");
        String postDate = res.getString("postDate");
        int postID = res.getInt("ID");
        ArrayList<String> likers = getPostLikers(postID);
        PostObject answer = new PostObject(postID, userID, getUserName(userID), pictureAddress, caption, postDate);
        answer.setLikers(likers);
        ArrayList<CommentObject> comments = getPostComments(postID);
        answer.setComments(comments);
        return answer;
    }

    private ArrayList<String> getPostLikers (int postID) throws SQLException // Getting likers of a post.
    {
        ArrayList<String> answer = new ArrayList<>();
        String query = "select * from Likes where postID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, postID);
        ResultSet res = preparedStatement.executeQuery();
        while(res.next())
        {
            int userID = res.getInt("userID");
            answer.add(getUserName(userID));
        }
        return answer;
    }

    private ArrayList<CommentObject> getPostComments(int postID) throws SQLException // Getting comments of a post.
    {
        String query = "select * from Comments where postID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, postID);
        ResultSet res = preparedStatement.executeQuery();
        ArrayList<CommentObject> answer = new ArrayList<>();
        CommentObject commentObject;
        while(res.next())
        {
            commentObject = resultToComment(res);
            answer.add(commentObject);
        }
        return answer;
    }

    private CommentObject resultToComment(ResultSet res) throws SQLException
    {
        int ID = res.getInt("ID");
        int userID = res.getInt("userID");
        int postID = res.getInt("postID");
        String username = getUserName(userID);
        String commentDate = res.getString("commentDate");
        String commentText = res.getString("commentText");
        CommentObject commentObject = new CommentObject(ID, username, userID, postID, commentText, commentDate);
        return commentObject;
    }

    public int getFollowersNumber(String username) throws SQLException
    {
        int userID = getUserID(username);
        String query = "select * from RelationShip where followingID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        int answer = preparedStatement.executeUpdate();
        return answer;
    }

    public int getFollowingNumber(String username) throws SQLException
    {
        int userID = getUserID(username);
        String query = "select * rom RelationShip where followerID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        int answer = preparedStatement.executeUpdate();
        return answer;
    }

    public ArrayList<String> getFollowers(String username) throws SQLException
    {
        int userID = getUserID(username);
        String query = "select * from RelationShip where followingID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        ResultSet result = preparedStatement.executeQuery();
        ArrayList<String> answer = new ArrayList<>();
        while(result.next())
        {
            int targetID = result.getInt("followerID");
            answer.add(getUserName(targetID));
        }
        return answer;
    }

    public ArrayList<String> getFollowings(String username) throws  SQLException
    {
        int userID = getUserID(username);
        String query = "select * from RelationShip where followerID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<String> answer = new ArrayList<>();
        while(resultSet.next())
        {
            int targetID = resultSet.getInt("followingID");
            answer.add(getUserName(targetID));
        }
        return answer;
    }

    public ArrayList<String> getChattedUsers(String username) throws SQLException
    {
        ArrayList<String> answers = new ArrayList<>();
        int userID = getUserID(username);
        String query = "select * from DirectMessage where senderID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        ResultSet result = preparedStatement.executeQuery();
        while(result.next())
        {
            int targetID = result.getInt("receiverID");
            answers.add(getUserName(targetID));
        }
        query = "select * from DirectMessage where receiverID = ?;";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID);
        result = preparedStatement.executeQuery();
        while(result.next())
        {
            int targetID = result.getInt("receiverID");
            answers.add(getUserName(targetID));
        }
        answers = removeDuplicates(answers);
        return answers;
    }

    public ArrayList<DirectMessageObject> getAllChats(String username1, String username2) throws SQLException
    {
        int userID1 = getUserID(username1);
        int userID2 = getUserID(username2);
        ArrayList<DirectMessageObject> answer = new ArrayList<>();
        String query = "select * from DirectMessages where senderID = ? and receiverID = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID1);
        preparedStatement.setInt(2, userID2);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next())
        {
            DirectMessageObject directMessageObject = resultToDirectMessage(resultSet);
            answer.add(directMessageObject);
        }
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, userID2);
        preparedStatement.setInt(2, userID1);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next())
        {
            DirectMessageObject directMessageObject = resultToDirectMessage(resultSet);
            answer.add(directMessageObject);
        }
        return answer;
    }

    private ArrayList<DirectMessageObject> directMessageObjectsSorter(ArrayList<DirectMessageObject> input)
    {
        ArrayList<DirectMessageObject> answer = new ArrayList<>();
        ArrayList<Date> dates = new ArrayList<>();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        for (DirectMessageObject dm: input)
        {
            dates.add((Date) dateFormat.parse(dm.getMessageDate()));
        }
        Collections.sort(dates);
        for (Date d: dates)
        {
            for (int i = 0 ; i < input.size(); i++)
            {
                if (input.get(i).getMessageDate().equals(dateFormat.format((TemporalAccessor) d)))
                {
                    answer.add(input.get(i));
                    break;
                }
            }
        }
        return answer;
    }

    private DirectMessageObject resultToDirectMessage(ResultSet resultSet) throws SQLException
    {
        String senderUsername, receiverUsername, messageText, messageDate;
        int ID;
        if (resultSet.next()) {
            ID = resultSet.getInt("ID");
            senderUsername = getUserName(resultSet.getInt("senderID"));
            receiverUsername = getUserName(resultSet.getInt("receiverID"));
            messageText = resultSet.getString("messageText");
            messageDate = resultSet.getString("messageDate");
            DirectMessageObject answer = new DirectMessageObject(ID, senderUsername, receiverUsername, messageText, messageDate);
            return answer;
        }
        else
        {
            return null;
        }
    }

    private static <T> ArrayList<T> removeDuplicates(ArrayList<T> list)
    {

        Set<T> set = new LinkedHashSet<>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    @Override
    public void close() throws IOException {
        try {
            this.connection.close();
        }
        catch (SQLException exception)
        {
            exception.printStackTrace();
        }

    }
}
