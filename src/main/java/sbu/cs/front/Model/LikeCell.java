package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.classes.LikeObject;
import sbu.cs.front.Controller.LikeCellController;

import java.io.IOException;

public class LikeCell extends ListCell<String> {

    public LikeCellController likeCellController = null;

    {
        try {
            likeCellController = new LikeCellController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = likeCellController.getView();


    @Override
    protected void updateItem (String liker , boolean empty){

        super.updateItem(liker , empty);
        if (empty){
            setGraphic(null);
        }
        else {
            likeCellController.setUsername(liker);
            setGraphic(view);
        }
    }
}
