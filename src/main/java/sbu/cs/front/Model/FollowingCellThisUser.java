package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.FollowingItemThisUserController;

import java.io.IOException;

public class FollowingCellThisUser extends ListCell<String> {

    public FollowingItemThisUserController followingItemController = null;

    {
        try {
            followingItemController = new FollowingItemThisUserController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = followingItemController.getView();

    @Override
    protected void updateItem(String following, boolean empty)
    {
        super.updateItem(following, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            followingItemController.setUsername(following);
            setGraphic(view);
        }

    }





}
