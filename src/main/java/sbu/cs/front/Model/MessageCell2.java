package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.MessageItemUser2Controller;
import sbu.cs.classes.MessageObject;

import java.io.IOException;


public class MessageCell2 extends ListCell<MessageObject> {

    public MessageItemUser2Controller messageItemUser2Controller = null;

    {
        try {
            messageItemUser2Controller = new MessageItemUser2Controller();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = messageItemUser2Controller.getView();

    @Override
    protected void updateItem(MessageObject message, boolean empty)
    {
        super.updateItem(message, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            messageItemUser2Controller.setMessageText2(message.getMessage());
            setGraphic(view);
        }

    }
}
