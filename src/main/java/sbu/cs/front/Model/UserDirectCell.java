package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.DirectUserController;
import sbu.cs.classes.UserDirectObject;

import java.io.IOException;

public class UserDirectCell extends ListCell<UserDirectObject> {

    public DirectUserController directUserController = null;

    {
        try {
            directUserController = new DirectUserController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = directUserController.getView();

    @Override
    protected void updateItem(UserDirectObject user, boolean empty)
    {
        super.updateItem(user, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            directUserController.setUsername(user.getUsername());
            setGraphic(view);
        }

    }
}
