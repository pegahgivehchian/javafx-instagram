package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.FollowerItemThisUserController;

import java.io.IOException;

public class FollowerCellThisUser extends ListCell<String> {

    public FollowerItemThisUserController followerItemThisUserController = null;

    {
        try {
            followerItemThisUserController = new FollowerItemThisUserController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = followerItemThisUserController.getView();

    @Override
    protected void updateItem(String follower, boolean empty)
    {
        super.updateItem(follower, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            followerItemThisUserController.setUsername(follower);
            setGraphic(view);
        }

    }
}
