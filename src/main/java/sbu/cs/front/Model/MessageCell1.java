package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.MessageItemUser1Controller;
import sbu.cs.classes.MessageObject;

import java.io.IOException;


public class MessageCell1 extends ListCell<MessageObject>{

    public MessageItemUser1Controller messageItemUser1Controller = null;

    {
        try {
            messageItemUser1Controller = new MessageItemUser1Controller();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = messageItemUser1Controller.getView();

    @Override
    protected void updateItem(MessageObject message, boolean empty)
    {
        super.updateItem(message, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            messageItemUser1Controller.setMessageText1(message.getMessage());
            setGraphic(view);
        }

    }
}
