package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.classes.CommentObject;
import sbu.cs.front.Controller.CommentCellController;

import java.io.IOException;

public class CommentCell extends ListCell<CommentObject> {

    public CommentCellController commentCellController = null;

    {
        try {
            commentCellController = new CommentCellController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = commentCellController.getView();


    @Override
    protected void updateItem (CommentObject comment , boolean empty){

        super.updateItem(comment , empty);
        if (empty){
            setGraphic(null);
        }
        else {
            commentCellController.setCommentLabel(comment.getCommentText());
            commentCellController.setUsernameLabel(comment.getUsername());
            setGraphic(view);
        }
    }

}
