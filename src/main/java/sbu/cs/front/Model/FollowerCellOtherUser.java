package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.FollowerItemOtherUserController;

import java.io.IOException;

public class FollowerCellOtherUser extends ListCell<String> {

    public FollowerItemOtherUserController followerItemAnotherOneController = null;

    {
        try {
            followerItemAnotherOneController = new FollowerItemOtherUserController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = followerItemAnotherOneController.getView();

    @Override
    protected void updateItem(String follower, boolean empty)
    {
        super.updateItem(follower, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            followerItemAnotherOneController.setUsername(follower);
            setGraphic(view);
        }

    }
}
