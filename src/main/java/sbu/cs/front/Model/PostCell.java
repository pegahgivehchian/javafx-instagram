package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.classes.PostObject;
import sbu.cs.front.Controller.PostController;


public class PostCell extends ListCell<PostObject> {

    public final PostController postController = new PostController();
    final Node view = postController.getView();


    @Override
    protected void updateItem(PostObject post , boolean empty){

        super.updateItem(post,empty);
        if (empty){
            setGraphic(null);
        }
        else{
            postController.setPost(post);
            setGraphic(view);
        }
    }
}
