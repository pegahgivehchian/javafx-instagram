package sbu.cs.front.Model;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import sbu.cs.front.Controller.FollowingItemOtherUserController;

import java.io.IOException;

public class FollowingCellOtherUser extends ListCell<String> {

    public FollowingItemOtherUserController followingItemAnotherOneController = null;

    {
        try {
            followingItemAnotherOneController = new FollowingItemOtherUserController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    final Node view = followingItemAnotherOneController.getView();

    @Override
    protected void updateItem(String following, boolean empty)
    {
        super.updateItem(following, empty);

        if(empty)
        {
            setGraphic(null);
        }
        else {
            followingItemAnotherOneController.setUsername(following);
            setGraphic(view);
        }

    }
}
