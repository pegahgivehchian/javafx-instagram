package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import sbu.cs.classes.UserDirectObject;
import sbu.cs.front.Model.UserDirectCell;

import java.net.URL;
import java.util.ResourceBundle;

public class DirectPageController implements Initializable {

    @FXML
    private Label nameLabel;

    @FXML
    private Label bioLabel;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXListView directUserList;

    @FXML
    private Circle circlePic;

    @FXML
    private JFXButton myProfileButton;

    @FXML
    private JFXButton directBtn;

    @FXML
    private JFXButton exploreBtn;

    @FXML
    private JFXButton homeBtn;

    @FXML
    private JFXTextField searchedUsername;

    @FXML
    private Text foundedUsername;

    @FXML
    private JFXButton profileButton;

    @FXML
    private JFXButton settingsBtn;

    @FXML
    private JFXButton editProfileBtn;

    ObservableList<UserDirectObject> uList = FXCollections.<UserDirectObject>observableArrayList(

            //inja bayad ba ye halqe for az 1 ta tedade user ha, hey objecte user new konim va esme bhesh bdim.

            new UserDirectObject("farimah"),
            new UserDirectObject("mobin"),
            new UserDirectObject("pegah")
    );


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        directUserList.setItems(uList);
        directUserList.setCellFactory(param -> new UserDirectCell());
        try {
            SideMenuController sideMenuController = new SideMenuController(homeBtn,myProfileButton,exploreBtn,directBtn,settingsBtn,editProfileBtn,circlePic,bioLabel,nameLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
