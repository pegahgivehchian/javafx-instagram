package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EditProfileController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton btnChangeUsername;

    @FXML
    private JFXButton btnChangeProfilePhoto;

    @FXML
    private JFXButton btnRemoveProfilePhoto;

    @FXML
    private JFXButton btnChangeBio;

    @FXML
    void changeBio(ActionEvent event) {

        btnChangeBio.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    ChangeBioController changeBioController = new ChangeBioController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/ChangeBio.fxml"));
                    loader.setController(changeBioController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });
    }

    @FXML
    void changeProfilePhoto(ActionEvent event) {

        FileChooser fileChooser = new FileChooser();

        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("All Images", "*.*"));
        File f = fileChooser.showOpenDialog(null);
        f.getAbsolutePath(); //in dastor adrese ax ro migire
        //chejori be onvane ax profile set beshe?

    }

    @FXML
    void changeUsername(ActionEvent event) {

        btnChangeUsername.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    ChangeUsernameController changeUsernameController = new ChangeUsernameController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/ChangeUsername.fxml"));
                    loader.setController(changeUsernameController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));
                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });
    }

    @FXML
    void removeProfilePhoto(ActionEvent event) {

    }









    @Override
    public void initialize(URL location, ResourceBundle resources) {

        assert btnChangeUsername != null : "fx:id=\"btnChangeUsername\" was not injected: check your FXML file 'EditProfile.fxml'.";
        assert btnChangeProfilePhoto != null : "fx:id=\"btnChangeProfilePhoto\" was not injected: check your FXML file 'EditProfile.fxml'.";
        assert btnRemoveProfilePhoto != null : "fx:id=\"btnRemoveProfilePhoto\" was not injected: check your FXML file 'EditProfile.fxml'.";
        assert btnChangeBio != null : "fx:id=\"btnChangeBio\" was not injected: check your FXML file 'EditProfile.fxml'.";
    }
}
