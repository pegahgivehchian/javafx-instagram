package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MessageItemUser2Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private HBox box2;

    @FXML
    private JFXTextArea messageText2;

    public MessageItemUser2Controller()throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../sample/MessageItemUser2.fxml"));
        loader.setController(this);
        box2 = loader.load();

    }

    public void setMessageText2(String messageText2){
        this.messageText2.setText(messageText2);
    }

    public JFXTextArea getMessageText2() {
        return messageText2;
    }


    public Node getView(){
        return box2;
    }

}
