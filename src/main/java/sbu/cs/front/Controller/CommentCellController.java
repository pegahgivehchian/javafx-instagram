package sbu.cs.front.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class CommentCellController {

    private AnchorPane anchorPane;

    @FXML
    private Label usernameLabel;

    @FXML
    private Label commentLabel;

    public CommentCellController() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/commentCell.fxml"));
        loader.setController(this);
        anchorPane = loader.load();
    }
    public Label getUsernameLabel() {
        return usernameLabel;
    }

    public void setUsernameLabel(String usernameLabel) {
        this.usernameLabel.setText(usernameLabel);
    }

    public Label getCommentLabel() {
        return commentLabel;
    }

    public void setCommentLabel(String commentLabel) {
        this.commentLabel.setText(commentLabel);
    }

    public Node getView(){
        return anchorPane;
    }
}
