package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import sbu.cs.classes.CommentObject;
import sbu.cs.front.GraphicFunctions;
import sbu.cs.front.Model.CommentCell;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class CommentsPageController  implements Initializable {

    private AnchorPane anchorPane;

    @FXML
    private TextField commentArea;

    @FXML
    private JFXButton sendBtn;

    @FXML
    private JFXListView commentsListView;

    private ArrayList<CommentObject> comments;

    ObservableList<CommentObject> cList ;

    public CommentsPageController(ArrayList<CommentObject> comments) {
        this.comments = comments;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        cList =  FXCollections.<CommentObject>observableArrayList(
        );
        for (CommentObject comment : comments){
            cList.add(comment);
        }
        GraphicFunctions.designButton(sendBtn);
        commentsListView.setItems(cList);
        commentsListView.setCellFactory(param -> new CommentCell());
    }


    public TextField getCommentArea() {
        return commentArea;
    }

    public void setCommentArea(TextField commentArea) {
        this.commentArea = commentArea;
    }

    public JFXButton getSendBtn() {
        return sendBtn;
    }

    public void setSendBtn(JFXButton sendBtn) {
        this.sendBtn = sendBtn;
    }

    public Node getView(){
        return anchorPane;
    }


}
