package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FollowingItemOtherUserController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private HBox box;

    @FXML
    private Label username;

    @FXML
    private JFXButton btnGoToProfile;

    public FollowingItemOtherUserController()throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/FollowingItemAnotherOne.fxml"));
        loader.setController(this);
        box = loader.load();

    }

    @FXML
    void goToProfile(ActionEvent event) {

    }

    public void setUsername(String username){
        this.username.setText(username);
    }

    public Label getUsername() {
        return username;
    }

    public Node getView(){
        return box;
    }
}
