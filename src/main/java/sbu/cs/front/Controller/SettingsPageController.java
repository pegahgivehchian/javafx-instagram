package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsPageController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXButton btnChangePassword;

    @FXML
    private JFXButton btnLogOut;

    @FXML
    void actionChangePassword(ActionEvent event) {

        btnChangePassword.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    ChangePasswordController changePasswordController = new ChangePasswordController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/ChangePassword.fxml"));
                    loader.setController(changePasswordController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });

    }

    @FXML
    void actionLogOut(ActionEvent event) {

        btnLogOut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    LogOutController logOutController = new LogOutController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/LogOut.fxml"));
                    loader.setController(logOutController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert anchorPane != null : "fx:id=\"anchorPane\" was not injected: check your FXML file 'Setting.fxml'.";
        assert btnChangePassword != null : "fx:id=\"btnChangePassword\" was not injected: check your FXML file 'Setting.fxml'.";
        assert btnLogOut != null : "fx:id=\"btnLogOut\" was not injected: check your FXML file 'Setting.fxml'.";
    }


}
