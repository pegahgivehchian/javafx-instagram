package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MessageItemUser1Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private HBox box1;

    @FXML
    private JFXTextArea messageText1;

    public MessageItemUser1Controller()throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../sample/MessageItemUser1.fxml"));
        loader.setController(this);
        box1 = loader.load();

    }

    public void setMessageText1(String messageText1){
        this.messageText1.setText(messageText1);
    }

    public JFXTextArea getMessageText1() {
        return messageText1;
    }


    public Node getView(){
        return box1;
    }

}
