package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ChangeBioController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton btnDone;

    @FXML
    private JFXButton btnBack;

    @FXML
    private Label whatIsDone;

    @FXML
    private JFXTextArea changeBioField;

    @FXML
    void back(ActionEvent event) {

        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    EditProfileController editProfileController = new EditProfileController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/EditProfile.fxml"));
                    loader.setController(editProfileController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });
    }

    @FXML
    void done(ActionEvent event) {

        changeBioField.getText();
        //texte bala ro bayad zakhire konim tuye database
        whatIsDone.setText("successfully done!");
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        assert changeBioField != null : "fx:id=\"changeBioField\" was not injected: check your FXML file 'ChangeBio.fxml'.";
        assert btnDone != null : "fx:id=\"btnDone\" was not injected: check your FXML file 'ChangeBio.fxml'.";
        assert btnBack != null : "fx:id=\"btnBack\" was not injected: check your FXML file 'ChangeBio.fxml'.";
        assert whatIsDone != null : "fx:id=\"whatIsDone\" was not injected: check your FXML file 'ChangeBio.fxml'.";
    }
}
