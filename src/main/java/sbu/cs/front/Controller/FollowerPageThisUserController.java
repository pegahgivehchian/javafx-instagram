package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import sbu.cs.front.Model.FollowerCellThisUser;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FollowerPageThisUserController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXListView followersListView;

//    ObservableList<String> flList = FXCollections.<String>observableArrayList(
//
//            new FollowerObject("farimah"),
//            new FollowerObject("mobin"),
//            new FollowerObject("pegah")
//    );

    private ObservableList<String> flList;
    private ArrayList<String> followers;

    public FollowerPageThisUserController(ArrayList<String> followers) {
        this.followers = followers;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        flList = FXCollections.<String>observableArrayList();
        for (String f : followers){
            flList.add(f);
        }
        followersListView.setItems(flList);
        followersListView.setCellFactory(param -> new FollowerCellThisUser());
    }
}
