package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sbu.cs.FileTransferProcessor;
import sbu.cs.TaskManager;
import sbu.cs.Translator;
import sbu.cs.Whisper;
import sbu.cs.classes.PostObject;
import sbu.cs.front.Model.PostCell;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ProfileOtherUserController implements Initializable {

    @FXML
    private Label nameLabel;

    @FXML
    private Label bioLabel;

    @FXML
    private Circle circlePic;

    @FXML
    private JFXButton myProfileButton;

    @FXML
    private JFXButton directBtn;

    @FXML
    private JFXButton exploreBtn;

    @FXML
    private JFXButton homeBtn;

    @FXML
    private JFXTextField searchedUsername;

    @FXML
    private Text foundedUsername;

    @FXML
    private JFXButton profileButton;

    @FXML
    private JFXButton settingsBtn;

    @FXML
    private JFXButton editProfileBtn;

    @FXML
    private Circle profilePhoto;


    @FXML
    private Text profileName;

    @FXML
    private JFXButton followingsList;

    @FXML
    private JFXButton followersList;

    @FXML
    private Text followersNum;

    @FXML
    private Text followingsNum;

    @FXML
    private Text postsNum;

    @FXML
    private JFXListView<PostObject> postsListView;

    @FXML
    private JFXButton btnFollow;

    @FXML
    private JFXButton unfollowBtn;

    @FXML
    private Text whatsDone;

    @FXML
    private Label profileBioLabel;


    @FXML
    void btnFollowAction(ActionEvent event) {

        Whisper outPutWhisper = new Whisper();
        outPutWhisper.setRequest("Follow Request");
        Map data = new HashMap();
        data.put("follower" , TaskManager.getLoggedInUser());
        data.put("followed" , thisUser);
        outPutWhisper.setData(data);
        TaskManager taskManager = new TaskManager(outPutWhisper);
        Whisper responseWhisper = taskManager.getResponseWhisper();

        if( responseWhisper.getRequest().equals("Task Done")) {
            btnFollow.setVisible(false);
            unfollowBtn.setVisible(true);
        }

    }



    @FXML
    void btnFollowersAction(ActionEvent event) throws IOException {
        FollowerPageOtherUserController followerPageOtherUserController = new FollowerPageOtherUserController(followers);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/FollowerListAnotherOne.fxml"));
        loader.setController(followerPageOtherUserController);
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @FXML
    void btnFollowingsAction(ActionEvent event) throws IOException {
        FollowingPageOtherUserController followingPageOtherUserController = new FollowingPageOtherUserController(followings);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/FollowingListAnotherOne.fxml"));
        loader.setController(followingPageOtherUserController);
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @FXML
    void unfollowAction(ActionEvent event) {
        Whisper outPutWhisper = new Whisper();
        outPutWhisper.setRequest("Unfollow Request");
        Map data = new HashMap();
        data.put("follower" , TaskManager.getLoggedInUser());
        data.put("followed" , thisUser);
        outPutWhisper.setData(data);
        TaskManager taskManager = new TaskManager(outPutWhisper);
        Whisper responseWhisper = taskManager.getResponseWhisper();

        if( responseWhisper.getRequest().equals("Task Done")) {
            unfollowBtn.setVisible(false);
            btnFollow.setVisible(true);
        }
    }

    private ObservableList<PostObject> pList;
    private Translator translator;
    private String thisUser;
    private ArrayList<String> followers;
    private ArrayList<String> followings;

    public ProfileOtherUserController(String user) {
        thisUser = user;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            initializePostList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SideMenuController sideMenuController = new SideMenuController(homeBtn,myProfileButton,exploreBtn,directBtn,settingsBtn,editProfileBtn,circlePic,bioLabel,nameLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        postsListView.setItems(this.pList);
        postsListView.setCellFactory(param -> new PostCell());
        profileName.setText(thisUser);
        setFollowState();
        try {
            initProfileInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializePostList() throws Exception {

        translator = new Translator();
        TaskManager.createNewDirectory(thisUser);
        String request = "Retrieving Posts";
        Map<String, String> data = new HashMap<>();
        data.put("username", thisUser);
        Whisper req = new Whisper(request , data);
        TaskManager taskManager = new TaskManager(req);
        String response = taskManager.getResponseWhisper().getRequest();
        FileTransferProcessor ftp = new FileTransferProcessor(TaskManager.socket);
        if (response.equals("No Record Found")){
            pList =  FXCollections.<PostObject>observableArrayList();
        }
        else {
            DataInputStream inputStream = new DataInputStream(TaskManager.socket.getInputStream());
            ArrayList<PostObject> posts = new ArrayList<>();
            posts = translator.stringToPosts(inputStream.readUTF());
            postsNum.setText(Integer.toString(posts.size()));
            pList =  FXCollections.<PostObject>observableArrayList();
            for (PostObject post: posts){
                String input = inputStream.readUTF();
                Whisper whisper = translator.stringToWhisper(input);
                String fileName = whisper.getValueWithKey("fileName");
                String fileSize = whisper.getValueWithKey("fileSize");
                String address = "./src/main/resources/Posts/" + thisUser+ "/" + fileName;
                ftp.fileReceiver(address , Integer.parseInt(fileSize));
                address = "/Posts/" + thisUser + "/" + fileName;
                post.setPictureAddress(address);
                System.out.println("address = " + address);
                pList.add(post);
            }
        }
    }

    public void setFollowState(){

        Whisper outPutWhisper = new Whisper();
        outPutWhisper.setRequest("Get Follow State");
        Map data = new HashMap();
        data.put("follower" , TaskManager.getLoggedInUser());
        data.put("followed" , thisUser);
        outPutWhisper.setData(data);
        TaskManager taskManager = new TaskManager(outPutWhisper);
        Whisper responseWhisper = taskManager.getResponseWhisper();

        if( responseWhisper.getValueWithKey("isFollowed").equals("true")) {
        btnFollow.setVisible(false);
        unfollowBtn.setVisible(true);
        }
        else{
            btnFollow.setVisible(true);
            unfollowBtn.setVisible(false);
        }
    }

    private void initProfileInfo() throws Exception {
        Whisper req = new Whisper();
        req.setRequest("Profile Info");
        Map<String, String> data = new HashMap();
        data.put("username", thisUser);
        data.put("noProfilePic" , "false");
        req.setData(data);
        TaskManager taskManager = new TaskManager(req);
        Whisper inputWhisper = taskManager.getResponseWhisper();
        FileTransferProcessor fileTransferProcessor = new FileTransferProcessor(TaskManager.socket);
        if (inputWhisper.getRequest().equals("Profile Info")) {
            profileBioLabel.setText(inputWhisper.getValueWithKey("bio"));
            profileBioLabel.setWrapText(true);
            nameLabel.setText(thisUser);
            String fileName = inputWhisper.getValueWithKey("fileName");
            String fileSize = inputWhisper.getValueWithKey("fileSize");
            if (fileName != null) {
                System.out.println("filename : " + fileName);
                String fileAddress = "./src/main/resources/ProfilePics/" + fileName;
                fileTransferProcessor.fileReceiver(fileAddress, Integer.valueOf(fileSize));
                fileAddress = "/ProfilePics/" + fileName;
                Image image = new Image(fileAddress);
                profilePhoto.setFill(new ImagePattern(image));
            }
            followers = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followers"));
            followings = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followings"));
            followersNum.setText(Integer.toString(followers.size()));
            followingsNum.setText(Integer.toString(followings.size()));
        }
    }


    public void setUsername(Text username) {
        this.profileName = username;
    }

    public ArrayList<String> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<String> followers) {
        this.followers = followers;
    }

    public ArrayList<String> getFollowings() {
        return followings;
    }

    public void setFollowings(ArrayList<String> followings) {
        this.followings = followings;
    }
}
