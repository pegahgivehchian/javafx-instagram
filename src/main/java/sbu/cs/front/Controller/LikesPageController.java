package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import sbu.cs.classes.LikeObject;
import sbu.cs.front.Model.LikeCell;


import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class LikesPageController implements Initializable {

    @FXML
    private JFXListView likesListView;

    private ArrayList<String> likers;

    ObservableList<String> lList ;

    public LikesPageController(ArrayList<String> likers) {
        this.likers = likers;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lList = FXCollections.<String>observableArrayList(
        );
        for (String liker : likers){
            lList.add(liker);
        }
        likesListView.setItems(lList);
        likesListView.setCellFactory(param -> new LikeCell());
    }
}
