package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sbu.cs.FileTransferProcessor;
import sbu.cs.TaskManager;
import sbu.cs.Translator;
import sbu.cs.Whisper;

import java.io.DataInputStream;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class NewPostPageController implements Initializable {

    File f;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton btnChoosePicture;

    @FXML
    private JFXTextArea captionField;

    @FXML
    private JFXButton btnDone;

    @FXML
    private Label errorLBL;

    @FXML
    void choosePictureAction(ActionEvent event) {

        FileChooser fileChooser = new FileChooser();

        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("All Images", "*.*"));
        f = fileChooser.showOpenDialog(null);

    }

    @FXML
    void doneAction(ActionEvent event) throws Exception {

        if (f.equals(null)){
            errorLBL.setText("Please choose a picture!");
        }
        else {
            String request = "Add Post";
            Map<String, String> data = new HashMap<>();
            data.put("username", TaskManager.getLoggedInUser());
            data.put("caption", captionField.getText());
            data.put("fileName", f.getName());
            data.put("fileSize", Integer.toString((int) f.length()));
            Whisper req = new Whisper(request, data);
            TaskManager taskManager = new TaskManager(req);
            Whisper responseWhisper = taskManager.getResponseWhisper();
            if (responseWhisper.getRequest().equals("Task Done")) {
                FileTransferProcessor fileTransferProcessor = new FileTransferProcessor(TaskManager.socket);
                fileTransferProcessor.fileSender(f);
                System.out.println("img File sent to service!");
                System.out.println("Waiting for response");
                DataInputStream dataInputStream = new DataInputStream(TaskManager.socket.getInputStream());
                String socketInput = dataInputStream.readUTF();
                Thread.sleep(30);
                Translator translator = new Translator();
                Whisper inputWhisper = translator.stringToWhisper(socketInput);
                if (inputWhisper.getRequest().equals("Error")) {
                    errorLBL.setText(inputWhisper.getValueWithKey("Error Status"));
                } else if (responseWhisper.getRequest().equals("Task Done")) {
                    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    stage.hide();
                }
            }
        }



    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        assert btnChoosePicture != null : "fx:id=\"btnChoosePicture\" was not injected: check your FXML file 'NewPostPage.fxml'.";
        assert captionField != null : "fx:id=\"captionField\" was not injected: check your FXML file 'NewPostPage.fxml'.";
        assert btnDone != null : "fx:id=\"btnDone\" was not injected: check your FXML file 'NewPostPage.fxml'.";


    }
}
