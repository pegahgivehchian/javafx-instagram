package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import sbu.cs.front.Model.FollowingCellOtherUser;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FollowingPageOtherUserController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXListView followingsListView;

    private ObservableList<String> followingList;
    private ArrayList<String> following;

//    ObservableList<FollowingObject> flList = FXCollections.<FollowingObject>observableArrayList(
//
//            //inja bayad ba ye halqe for az 1 ta tedade followiga, hey objecte following new konim va esme bhesh bdim.
//
//            new FollowingObject("farimah"),
//            new FollowingObject("mobin"),
//            new FollowingObject("pegah")
//    );


    public FollowingPageOtherUserController(ArrayList<String> following) {
        this.following = following;
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        followingList = FXCollections.<String>observableArrayList();
        for (String f : following){
            followingList.add(f);
        }
        followingsListView.setItems(followingList);
        followingsListView.setCellFactory(param -> new FollowingCellOtherUser());

    }
}
