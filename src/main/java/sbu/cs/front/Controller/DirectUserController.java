package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DirectUserController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private HBox box;

    @FXML
    private Label username;

    @FXML
    private JFXButton btnGoToChat;

    @FXML
    void goToChatAction(ActionEvent event) {

        MessagesPageController messagesPageController = new MessagesPageController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/MessagesPage.fxml"));
        loader.setController(messagesPageController);
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage thisStage = new Stage();
        thisStage.setScene(new Scene(root));
        thisStage.show();
    }

    public DirectUserController()throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/DirectUser.fxml"));
        loader.setController(this);
        box = loader.load();

    }

    public void setUsername(String username){
        this.username.setText(username);
    }

    public Label getUsername() {
        return username;
    }


    public Node getView(){
        return box;
    }







}
