package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sbu.cs.TaskManager;
import sbu.cs.Whisper;
import sbu.cs.front.GraphicFunctions;


import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ExplorePageController implements Initializable {

    @FXML
    private Label nameLabel;

    @FXML
    private Label bioLabel;

    @FXML
    private Circle circlePic;

    @FXML
    private JFXButton editProfileBtn;

    @FXML
    private JFXButton settingsBtn;

    @FXML
    private JFXButton myProfileButton;

    @FXML
    private JFXButton directBtn;

    @FXML
    private JFXButton exploreBtn;

    @FXML
    private JFXButton homeBtn;

    @FXML
    private JFXTextField searchedUsername;

    @FXML
    private Text foundedUsername;

    @FXML
    private JFXButton profileButton;

    @FXML
    private Circle profileImage;

    @FXML
    private JFXButton searchButton;

    @FXML
    private Label errorLBL;

    @FXML
    void goToProfileAction(ActionEvent event) {
        String username = searchedUsername.getText();
        System.out.println("username : "+username);

        if (username != ""){

            ProfileOtherUserController profileOtherUserController = new ProfileOtherUserController(username);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/ProfileAnotherOne.fxml"));
            loader.setController(profileOtherUserController);
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Node node = (Node) event.getSource();
            Stage thisStage = (Stage) node.getScene().getWindow();
            thisStage.setScene(new Scene(root));
        }

    }


    @FXML
    void searchAction(ActionEvent event) {

        String username = searchedUsername.getText();
        String request = "Check Username availability";
        Map<String,String> data= new HashMap<>();
        data.put("username", username);
        Whisper req = new Whisper(request,data);
        TaskManager taskManager = new TaskManager(req);
        Whisper responseWhisper = taskManager.getResponseWhisper();
        if (responseWhisper.getValueWithKey("isAvailable").equals("true")){
            foundedUsername.setText(username);
            errorLBL.setText("");
        }
        else{
            foundedUsername.setText("");
            errorLBL.setText("User not found");
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GraphicFunctions.designButton(searchButton);
        GraphicFunctions.designButton(profileButton);
        try {
            SideMenuController sideMenuController = new SideMenuController(homeBtn,myProfileButton,exploreBtn,directBtn,settingsBtn,editProfileBtn,circlePic,bioLabel,nameLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Label getErrorLBL() {
        return errorLBL;
    }

    public void setErrorLBL(Label errorLBL) {
        this.errorLBL = errorLBL;
    }
}
