package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sbu.cs.classes.PostObject;
import sbu.cs.front.GraphicFunctions;

import java.io.IOException;


public class PostController {

    private AnchorPane anchorPane;

    @FXML
    private ImageView image;

    @FXML
    private Label caption;

    @FXML
    private Label likesNumber;

    @FXML
    private JFXButton commentsBtn;

    @FXML
    private JFXButton likesBtn;

    PostObject thisPost;

    public PostController() {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Post.fxml"));
            loader.setController(this);
            anchorPane = loader.load();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        GraphicFunctions.designButton(commentsBtn);
        GraphicFunctions.designButton(likesBtn);
        initCommentBtn();
        initLikesBtn();
    }
    public void setPost(PostObject post){
        caption.setText(post.getCaption());
        image.setImage(post.getImage());
        likesNumber.setText(String.valueOf(post.getLikes()));
        thisPost = post;
    }

    public void initLikesBtn(){
        likesBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    LikesPageController likesPageController = new LikesPageController(thisPost.getLikers());
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/likesPage.fxml"));
                    loader.setController(likesPageController);
                    Parent root = loader.load();
                    Stage primaryStage = new Stage();
                    primaryStage.setScene(new Scene(root));
                    primaryStage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    public void initCommentBtn(){
        commentsBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    CommentsPageController commentsPageController = new CommentsPageController(thisPost.getComments());
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/commentsPage.fxml"));
                    loader.setController(commentsPageController);
                    Parent root = loader.load();
                    Stage primaryStage = new Stage();
                    primaryStage.setScene(new Scene(root));
                    primaryStage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });
    }
    public Node getView(){
        return anchorPane;
    }
}
