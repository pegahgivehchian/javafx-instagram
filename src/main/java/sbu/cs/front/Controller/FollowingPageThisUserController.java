package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import sbu.cs.front.Model.FollowingCellThisUser;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FollowingPageThisUserController implements Initializable {

    @FXML
    private JFXListView followingsListView;

//    ObservableList<String> flList = FXCollections.<FollowingObject>observableArrayList(
//
//            //inja bayad ba ye halqe for az 1 ta tedade followiga, hey objecte following new konim va esme bhesh bdim.
//
//            new FollowingObject("farimah"),
//            new FollowingObject("mobin"),
//            new FollowingObject("pegah")
//    );

    private ObservableList<String> flList;
    private ArrayList<String> followings;

    public FollowingPageThisUserController(ArrayList<String> followings) {
        this.followings = followings;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        flList = FXCollections.<String>observableArrayList();
        for (String f : followings){
            flList.add(f);
        }
        followingsListView.setItems(flList);
        followingsListView.setCellFactory(param -> new FollowingCellThisUser());
    }
}

