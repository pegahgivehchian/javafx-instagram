package sbu.cs.front.Controller;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import sbu.cs.classes.PostObject;
import sbu.cs.front.Model.PostCell;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
public class HomePageController implements Initializable {

    @FXML
    private Label nameLabel;

    @FXML
    private Label bioLabel;

    @FXML
    private Circle circlePic;

    @FXML
    private JFXButton homeBtn;

    @FXML
    private JFXButton myProfileButton;

    @FXML
    private JFXButton directBtn;

    @FXML
    private JFXButton exploreBtn;

    @FXML
    private JFXListView<PostObject> postListview;

    @FXML
    private Label bioLBL;

    @FXML
    private JFXButton newPostBtn;

    @FXML
    private JFXButton settingsBtn;

    @FXML
    private JFXButton editProfileBtn;



    List<String> comments =new ArrayList<>();

    ObservableList<PostObject> pList = FXCollections.<PostObject>observableArrayList(
//            new PostObject("hi\n caption \n ", comments ,null , "20"),
//            new PostObject("hi", comments , null , "20"),
//            new PostObject("hi", comments , null , "30")
    );

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            SideMenuController sideMenuController = new SideMenuController(homeBtn,myProfileButton,exploreBtn,directBtn,settingsBtn,editProfileBtn,circlePic,bioLabel,nameLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        postListview.setItems(pList);
        postListview.setCellFactory(param -> new PostCell());
    }

    public Label getBioLBL() {
        return bioLBL;
    }

    public void setBioLBL(Label bioLBL) {
        this.bioLBL = bioLBL;
    }
}