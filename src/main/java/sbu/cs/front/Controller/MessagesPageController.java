package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextArea;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import sbu.cs.classes.MessageObject;

import java.net.URL;
import java.util.ResourceBundle;

public class MessagesPageController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label username;

    @FXML
    private JFXTextArea typedMessageArea;

    @FXML
    private JFXButton btnSend;

    @FXML
    private JFXListView messagesList;

    @FXML
    void sendAction(ActionEvent event) {

    }


    ObservableList<MessageObject> mList = FXCollections.<MessageObject>observableArrayList(

            new MessageObject("farimah"),
            new MessageObject("mobin"),
            new MessageObject("pegah")
    );




    @Override
    public void initialize(URL location, ResourceBundle resources) {

        ///////////////////////////////////////////////
    }
}
