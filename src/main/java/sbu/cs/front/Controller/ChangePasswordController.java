package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ChangePasswordController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXTextArea newPassField;

    @FXML
    private JFXButton btnDone;

    @FXML
    private JFXButton btnBack;

    @FXML
    private Label whatIsDone;

    @FXML
    void back(ActionEvent event) {

        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    SettingsPageController settingsPageController = new SettingsPageController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Setting.fxml"));
                    loader.setController(settingsPageController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });

    }

    @FXML
    void done(ActionEvent event) {

        newPassField.getText();
        //texte bala ro bayad zakhire konim tuye database
        whatIsDone.setText("successfully done!");

    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        assert newPassField != null : "fx:id=\"newPassField\" was not injected: check your FXML file 'ChangePassword.fxml'.";
        assert btnDone != null : "fx:id=\"btnDone\" was not injected: check your FXML file 'ChangePassword.fxml'.";
        assert btnBack != null : "fx:id=\"btnBack\" was not injected: check your FXML file 'ChangePassword.fxml'.";
        assert whatIsDone != null : "fx:id=\"whatIsDone\" was not injected: check your FXML file 'ChangePassword.fxml'.";


    }
}
