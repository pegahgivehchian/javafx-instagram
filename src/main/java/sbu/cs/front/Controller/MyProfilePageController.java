package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sbu.cs.FileTransferProcessor;
import sbu.cs.TaskManager;
import sbu.cs.Translator;
import sbu.cs.Whisper;
import sbu.cs.classes.PostObject;
import sbu.cs.front.Model.PostCell;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class MyProfilePageController implements Initializable {


    @FXML
    private Label nameLabel;

    @FXML
    private Label bioLabel;

    @FXML
    private Circle circlePic;

    @FXML
    private JFXButton myProfileButton;

    @FXML
    private JFXButton directBtn;

    @FXML
    private JFXButton exploreBtn;

    @FXML
    private JFXButton homeBtn;

    @FXML
    private JFXListView<PostObject> postListview;

    @FXML
    private JFXButton followingsList;

    @FXML
    private JFXButton followersList;

    @FXML
    private Text followersNum;

    @FXML
    private Text followingsNum;

    @FXML
    private Text postsNum;

    @FXML
    private JFXButton settingsBtn;

    @FXML
    private JFXButton addPostBtn;


    @FXML
    private JFXButton editProfileBtn;


    @FXML
    void btnFollowersAction(ActionEvent event) throws IOException {

        FollowerPageThisUserController followerPageThisUserController = new FollowerPageThisUserController(followers);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/FollowersListThisUser.fxml"));
        loader.setController(followerPageThisUserController);
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @FXML
    void btnFollowingsAction(ActionEvent event) throws IOException {
        FollowingPageThisUserController followingPageThisUserController = new FollowingPageThisUserController(followings);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/FollowingListThisUser.fxml"));
        loader.setController(followingPageThisUserController);
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @FXML
    void addPostAction(ActionEvent event) throws IOException {

        NewPostPageController newPostPageController = new NewPostPageController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/NewPostPage.fxml"));
        loader.setController(newPostPageController);
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }


//    ObservableList<PostObject> pList = FXCollections.<PostObject>observableArrayList(
////            new PostObject("hi\n i'm pegah \n lalala", comments ,null , "20"),
////            new PostObject("hi", comments , null , "20"),
////            new PostObject("hi", comments , null , "30")
//    );

    private ObservableList<PostObject> pList;
    private Translator translator;
    private ArrayList<String> followers;
    private ArrayList<String> followings;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            initializePostList();
            SideMenuController sideMenuController = new SideMenuController(homeBtn,myProfileButton,exploreBtn,directBtn,settingsBtn,editProfileBtn,circlePic,bioLabel,nameLabel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        postListview.setItems(this.pList);
        postListview.setCellFactory(param -> new PostCell());

        initInfo();
    }

    public void initializePostList() throws Exception {

        translator = new Translator();
        TaskManager.createNewDirectory(TaskManager.getLoggedInUser());
        String request = "Retrieving Posts";
        Map<String, String> data = new HashMap<>();
        data.put("username", TaskManager.getLoggedInUser());
        Whisper req = new Whisper(request , data);
        TaskManager taskManager = new TaskManager(req);
        String response = taskManager.getResponseWhisper().getRequest();
        FileTransferProcessor ftp = new FileTransferProcessor(TaskManager.socket);
        if (response.equals("No Record Found")){
            pList =  FXCollections.<PostObject>observableArrayList();
        }
        else {
            DataInputStream inputStream = new DataInputStream(TaskManager.socket.getInputStream());
            ArrayList<PostObject> posts = new ArrayList<>();
            posts = translator.stringToPosts(inputStream.readUTF());
            postsNum.setText(Integer.toString(posts.size()));
            pList =  FXCollections.<PostObject>observableArrayList();
            for (PostObject post: posts){
                String input = inputStream.readUTF();
                Whisper whisper = translator.stringToWhisper(input);
                String fileName = whisper.getValueWithKey("fileName");
                String fileSize = whisper.getValueWithKey("fileSize");
                String address = "./src/main/resources/Posts/" + TaskManager.getLoggedInUser() + "/" + fileName;
                ftp.fileReceiver(address , Integer.parseInt(fileSize));
                address = "/Posts/" + TaskManager.getLoggedInUser() + "/" + fileName;
                post.setPictureAddress(address);
                pList.add(post);
            }
        }
    }

//    private void initProfileInfo() throws Exception {
//        Whisper req = new Whisper();
//        req.setRequest("Profile Info");
//        Map<String, String> data = new HashMap();
//        data.put("username", TaskManager.getLoggedInUser());
//        req.setData(data);
//        TaskManager taskManager = new TaskManager(req);
//        Whisper inputWhisper = taskManager.getResponseWhisper();
//        FileTransferProcessor fileTransferProcessor = new FileTransferProcessor(TaskManager.socket);
//        if (inputWhisper.getRequest().equals("Profile Info")) {
//            followers = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followers"));
//            followings = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followings"));
//            followersNum.setText(Integer.toString(followers.size()));
//            followingsNum.setText(Integer.toString(followings.size()));
//        }
//    }

    private void initInfo(){
        Whisper req = new Whisper();
        req.setRequest("Profile Info");
        Map<String,String> data = new HashMap();
        data.put("username",TaskManager.getLoggedInUser());
        data.put("noProfilePic" , "true");
        req.setData(data);
        System.out.println("waiting for response info");
        TaskManager taskManager = new TaskManager(req);
        Whisper inputWhisper = taskManager.getResponseWhisper();
        if (inputWhisper.getRequest().equals("Profile Info")){
            followers = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followers"));
            followings = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followings"));
            followersNum.setText(Integer.toString(followers.size()));
            followingsNum.setText(Integer.toString(followings.size()));
        }
    }

//    private void initProfileInfo() throws Exception {
//        Whisper req = new Whisper();
//        req.setRequest("Profile Info");
//        Map<String, String> data = new HashMap();
//        data.put("username", TaskManager.getLoggedInUser());
//        data.put("isLoggedInUser" , "true");
//        req.setData(data);
//        TaskManager taskManager = new TaskManager(req);
//        Whisper inputWhisper = taskManager.getResponseWhisper();
//        FileTransferProcessor fileTransferProcessor = new FileTransferProcessor(TaskManager.socket);
//        if (inputWhisper.getRequest().equals("Profile Info")) {
//            String fileName = inputWhisper.getValueWithKey("fileName");
//            String fileSize = inputWhisper.getValueWithKey("fileSize");
//            if (fileName != null) {
//                System.out.println("filename : " + fileName);
//                String fileAddress = "./src/main/resources/ProfilePics/" + fileName;
//                fileTransferProcessor.fileReceiver(fileAddress, Integer.valueOf(fileSize));
//                fileAddress = "/ProfilePics/" + fileName;
//                Image image = new Image(fileAddress);
//                profilePhoto.setFill(new ImagePattern(image));
//            }
//            followers = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followers"));
//            followings = translator.stringToStringArrayList(inputWhisper.getValueWithKey("followings"));
//            followersNum.setText(Integer.toString(followers.size()));
//            followingsNum.setText(Integer.toString(followings.size()));
//        }
//    }
}
