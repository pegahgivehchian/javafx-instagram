package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import sbu.cs.FileTransferProcessor;
import sbu.cs.TaskManager;
import sbu.cs.Whisper;
import sbu.cs.front.GraphicFunctions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SideMenuController {

    private Button homeButton;
    private Button myProfileButton;
    private Button exploreBtn;
    private Button directBtn;
    private Button settingsBtn;
    private JFXButton editProfileBtn;
    private Circle circle;
    private Label bioLabel;
    private Label nameLabel;

    public SideMenuController(Button homeButton, Button myProfileButton, Button exploreBtn, Button directBtn, Button settingsBtn, JFXButton editProfileBtn, Circle circle, Label bio, Label nameLabel) throws Exception {

        this.homeButton = homeButton;
        this.myProfileButton = myProfileButton;
        this.exploreBtn = exploreBtn;
        this.directBtn = directBtn;
        this.settingsBtn = settingsBtn;
        this.editProfileBtn = editProfileBtn;
        this.circle = circle;
        this.bioLabel = bio;
        this.nameLabel = nameLabel;

        GraphicFunctions.designButtonMenu(homeButton);
        GraphicFunctions.designButtonMenu(myProfileButton);
        GraphicFunctions.designButtonMenu(directBtn);
        GraphicFunctions.designButtonMenu(exploreBtn);

        initExploreButton();
        initHomeButton();
        initMyProfileButton();
        initSettingsButton();
        initEditProfileBtn();
        initDirectBtn();
        initMyProfileInfo();
    }


    public void initHomeButton() {
        homeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                HomePageController homePageController = new HomePageController();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/home.fxml"));
                loader.setController(homePageController);
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Node node = (Node) event.getSource();
                Stage thisStage = (Stage) node.getScene().getWindow();
                thisStage.setScene(new Scene(root));
            }
        });
    }

    public void initExploreButton() {
        exploreBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ExplorePageController explorePageController = new ExplorePageController();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Explore.fxml"));
                loader.setController(explorePageController);
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Node node = (Node) event.getSource();
                Stage thisStage = (Stage) node.getScene().getWindow();
                thisStage.setScene(new Scene(root));
            }
        });
    }

    public void initMyProfileButton() {
        myProfileButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                MyProfilePageController myProfilePageController = new MyProfilePageController();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/myProfile.fxml"));
                loader.setController(myProfilePageController);
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Node node = (Node) event.getSource();
                Stage thisStage = (Stage) node.getScene().getWindow();
                thisStage.setScene(new Scene(root));
            }
        });
    }

    public void initSettingsButton() {
        settingsBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SettingsPageController settingsPageController = new SettingsPageController();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Setting.fxml"));
                loader.setController(settingsPageController);
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage primaryStage = new Stage();
                primaryStage.setScene(new Scene(root));
                primaryStage.show();
            }
        });
    }

    public void initEditProfileBtn() {
        editProfileBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                EditProfileController editProfileController = new EditProfileController();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/EditProfile.fxml"));
                loader.setController(editProfileController);
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage primaryStage = new Stage();
                primaryStage.setScene(new Scene(root));
                primaryStage.show();
            }
        });
    }

    public void initDirectBtn() {
        directBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectPageController directPageController = new DirectPageController();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/DirectPage.fxml"));
                loader.setController(directPageController);
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Node node = (Node) event.getSource();
                Stage thisStage = (Stage) node.getScene().getWindow();
                thisStage.setScene(new Scene(root));
                thisStage.show();
            }
        });
    }

    private void initMyProfileInfo() throws Exception {
        Whisper req = new Whisper();
        req.setRequest("Profile Info");
        Map<String, String> data = new HashMap();
        data.put("username", TaskManager.getLoggedInUser());
        data.put("noProfilePic" , "false");
        req.setData(data);
        TaskManager taskManager = new TaskManager(req);
        Whisper inputWhisper = taskManager.getResponseWhisper();
        FileTransferProcessor fileTransferProcessor = new FileTransferProcessor(TaskManager.socket);
        if (inputWhisper.getRequest().equals("Profile Info")) {
            bioLabel.setText(inputWhisper.getValueWithKey("bio"));
            nameLabel.setText(TaskManager.getLoggedInUser());
            String fileName = inputWhisper.getValueWithKey("fileName");
            String fileSize = inputWhisper.getValueWithKey("fileSize");
            if (fileName != null) {
                System.out.println("filename : " + fileName);
                String fileAddress = "./src/main/resources/ProfilePics/" + fileName;
                fileTransferProcessor.fileReceiver(fileAddress, Integer.valueOf(fileSize));
                fileAddress = "/ProfilePics/" + fileName;
                Image image = new Image(fileAddress);
                circle.setFill(new ImagePattern(image));
            }
        }
    }
}
