package sbu.cs.front.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;


public class LikeCellController {

    private AnchorPane anchorPane;


    @FXML
    private Label username;

    public LikeCellController() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/likeCell.fxml"));
        loader.setController(this);
        anchorPane = loader.load();
    }

    public Label getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username.setText(username);
    }

    public Node getView(){
        return anchorPane;
    }
}
