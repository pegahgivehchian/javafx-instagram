package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LogOutController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton btnYes;

    @FXML
    private JFXButton btnNo;

    @FXML
    void noAction(ActionEvent event) {

        btnNo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    SettingsPageController settingsPageController = new SettingsPageController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Setting.fxml"));
                    loader.setController(settingsPageController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });

    }

    @FXML
    void yesAction(ActionEvent event) {

        btnYes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    LogInController logInController = new LogInController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/LogIn.fxml"));
                    loader.setController(logInController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.hide();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.show();

                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });

    }






    @Override
    public void initialize(URL location, ResourceBundle resources) {

        assert btnYes != null : "fx:id=\"btnYes\" was not injected: check your FXML file 'LogOut.fxml'.";
        assert btnNo != null : "fx:id=\"btnNo\" was not injected: check your FXML file 'LogOut.fxml'.";
    }
}
