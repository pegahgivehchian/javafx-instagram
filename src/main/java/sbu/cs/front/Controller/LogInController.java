package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sbu.cs.TaskManager;
import sbu.cs.Whisper;
import sbu.cs.front.GraphicFunctions;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class LogInController implements Initializable {


    @FXML
    private JFXTextField fieldUsername;

    @FXML
    private JFXTextField fieldPassword;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private JFXButton btnSignUp;

    @FXML
    private Label errorLabel;

    @FXML
    private JFXButton followingsList;

    @FXML
    private JFXButton followersList;

    @FXML
    private Text followersNum;

    @FXML
    private Text followingsNum;

    @FXML
    private Text postsNum;

    @FXML
    void btnFollowersAction(ActionEvent event) {

    }

    @FXML
    void clickLoginAction(MouseEvent event) throws IOException {

        try {
            if (fieldUsername.getText().isEmpty() || fieldPassword.getText().isEmpty()) {
                errorLabel.setText("Fill all the blank spaces!");
            } else {
                String request = "Sign In";
                Map<String, String> data = new HashMap<>();
                data.put("username", fieldUsername.getText());
                data.put("password", fieldPassword.getText());
                Whisper req = new Whisper(request, data);
                //
                TaskManager taskManager = new TaskManager(req);
                Whisper responseWhisper = taskManager.getResponseWhisper();
                if (responseWhisper.getRequest().equals("Task Done")) {
                    TaskManager.setLoggedInUser(fieldUsername.getText());
                    HomePageController homePageController = new HomePageController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/home.fxml"));
                    loader.setController(homePageController);
                    Parent root = loader.load();
                    Node node = (Node) event.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));
                } else {
                    errorLabel.setText(responseWhisper.getValueWithKey("Error Status"));
                }

            }
//        HomePageController homePageController = new HomePageController();
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/home.fxml"));
//        loader.setController(homePageController);
//        Parent root = loader.load();
//        Node node = (Node) event.getSource();
//        Stage thisStage = (Stage) node.getScene().getWindow();
//        thisStage.setScene(new Scene(root));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    void clickSignupAction(MouseEvent event) throws IOException {
        SignUpController signUpController = new SignUpController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/SignUp.fxml"));
        loader.setController(signUpController);
        Parent root = loader.load();
        Node node = (Node) event.getSource();
        Stage thisStage = (Stage) node.getScene().getWindow();
        thisStage.setScene(new Scene(root));

    }
//        try {
//            if((fieldUsername).toString().isEmpty() || fieldPassword.toString().isEmpty())
//            {
//                errorLabel.setText("enter username and password!");
//                errorLabel.setTextFill(Color.RED);
//            }
//
//            else if((fieldUsername).toString().isEmpty() == false && fieldPassword.toString().isEmpty() == false)
//            {
//                if(//agar hamchin esmi dakhele database nabashad
//                )
//                {
//                    errorLabel.setText("user not found!");
//                }
//
//                if( //agar esm mojud bashad vali password eshtebah bashad
//                )
//                {
//                    errorLabel.setText("wrong password! try again.");
//                }
//
//                if(//esmo pass dorost bashad
//                )
//                {
//                    //inja bayad varede safe shakhsi beshim
//                }
//            }
//
//        }
//        catch (Exception e){
//            System.out.println(e.getMessage());
//        }







    @Override
    public void initialize(URL location, ResourceBundle resources) {

        GraphicFunctions.designLoginBtn(btnSignUp);
        GraphicFunctions.designLoginBtn(btnLogin);

    }


}

