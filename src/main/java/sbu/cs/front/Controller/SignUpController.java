package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sbu.cs.TaskManager;
import sbu.cs.Whisper;
import sbu.cs.front.GraphicFunctions;


import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class SignUpController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXTextField fieldUsername2;

    @FXML
    private JFXTextField fieldPassword2;

    @FXML
    private JFXTextField fieldEmail;

    @FXML
    private JFXButton btnSignUp2;

    @FXML
    private Label errorLabel2;

    @FXML
    public void clickSignupAction2(MouseEvent mouseEvent) throws IOException {


        try {
            if (fieldUsername2.getText().isEmpty() || fieldPassword2.getText().isEmpty() || fieldEmail.getText().isEmpty()) {
                errorLabel2.setText("Fill all the blank spaces!");
            }
            else{
                String request = "Sign Up";
                Map<String , String> data = new HashMap<>();
                data.put("username", fieldUsername2.getText());
                data.put("password", fieldPassword2.getText());
                data.put("email", fieldEmail.getText());
                Whisper req = new Whisper(request, data);
                //TaskManager.createSocket();
                TaskManager taskManager = new TaskManager(req);
                Whisper response = taskManager.getResponseWhisper();
                if (response.getRequest().equals("Task Done")){
                    TaskManager.setLoggedInUser(fieldUsername2.getText());
                    HomePageController homePageController = new HomePageController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/home.fxml"));
                    loader.setController(homePageController);
                    Parent root = loader.load();
                    Node node = (Node) mouseEvent.getSource();
                    Stage thisStage = (Stage) node.getScene().getWindow();
                    thisStage.setScene(new Scene(root));
                }
                else {
                    errorLabel2.setText(response.getValueWithKey("Error Status"));
                }

            }
//            else if((fieldUsername2).toString().isEmpty() == false && fieldPassword2.toString().isEmpty() == false)
//            {
//                if( //agar hamchin usernameE dakhele database az ghabl bashad
//                )
//                {
//                    errorLabel2.setText("you can't put this username! try another one.");
//                }
//
//            }
//        }
//

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

        @Override
    public void initialize(URL location, ResourceBundle resources) {

        GraphicFunctions.designLoginBtn(btnSignUp2);


        assert fieldUsername2 != null : "fx:id=\"fieldUsername2\" was not injected: check your FXML file 'sample.fxml'.";
        assert fieldPassword2 != null : "fx:id=\"fieldPassword2\" was not injected: check your FXML file 'sample.fxml'.";
        assert btnSignUp2 != null : "fx:id=\"btnSignUp2\" was not injected: check your FXML file 'sample.fxml'.";
        assert errorLabel2 != null : "fx:id=\"errorLabel2\" was not injected: check your FXML file 'sample.fxml'.";
        assert fieldEmail != null : "fx:id=\"fieldEmail\" was not injected: check your FXML file 'sample.fxml'.";
    }

}

