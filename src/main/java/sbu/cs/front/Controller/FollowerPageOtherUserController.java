package sbu.cs.front.Controller;

import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import sbu.cs.front.Model.FollowerCellOtherUser;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FollowerPageOtherUserController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXListView followersListView;

//    ObservableList<FollowerObject> flList = FXCollections.<FollowerObject>observableArrayList(
//
//            //inja bayad ba ye halqe for az 1 ta tedade followiga, hey objecte following new konim va esme bhesh bdim.
//
//            new FollowerObject("farimah"),
//            new FollowerObject("mobin"),
//            new FollowerObject("pegah")
//    );

    private ObservableList<String> followerList;
    private ArrayList<String> followers;

    public FollowerPageOtherUserController(ArrayList<String> followers) {
        this.followers = followers;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        followerList = FXCollections.<String>observableArrayList();
        for (String f : followers){
            followerList.add(f);
        }
        followersListView.setItems(followerList);
        followersListView.setCellFactory(param -> new FollowerCellOtherUser());
    }


}


