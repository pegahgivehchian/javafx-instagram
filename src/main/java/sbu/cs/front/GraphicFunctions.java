package sbu.cs.front;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class GraphicFunctions {
    public static void designButtonMenu(Button button){

        DropShadow dropShadow = new DropShadow();
        dropShadow.setColor(Color.TURQUOISE);

        button.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        button.setEffect(dropShadow);
                        button.setStyle("-fx-background-color: #778DA9");
                    }
                });
        button.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        button.setEffect(null);
                        button.setStyle("-fx-background-color: #546e7a");
                    }
                });
    }


    public static void designButton (Button button){
        DropShadow dropShadow = new DropShadow();
        dropShadow.setColor(Color.TURQUOISE);

        button.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        button.setEffect(dropShadow);
                        button.setTextFill(Color.WHITE);
                        button.setStyle("-fx-background-color: #546e7a;");
                    }
                });
        button.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        button.setEffect(null);
                        //button.setStyle("-fx-background-color: #white");
                        button.setTextFill(Color.BLACK);
                        button.setStyle("-fx-background-color: white;");
                    }
                });
    }

    public static void designLoginBtn(Button button) {

        DropShadow dropShadow = new DropShadow();
        dropShadow.setColor(Color.TURQUOISE);

        button.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        button.setEffect(dropShadow);
                        button.setStyle("-fx-background-color: #d3d3d3");
                        button.setStyle("-fx-text-fill: #90a4ae");
                    }
                });
        button.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        button.setEffect(null);
                        button.setStyle("-fx-background-color:  #90a4ae");
                    }
                });
    }
}
