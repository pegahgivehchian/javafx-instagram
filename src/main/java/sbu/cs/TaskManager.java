package sbu.cs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class TaskManager {

    public static Socket socket;
    private Whisper req;
    private Translator translator;
    private String response;



    private Whisper responseWhisper;

    public TaskManager(Whisper req){
        this.req = req;
        contactService(req);
    }

    public static void createSocket() throws IOException {
        socket = new Socket("localhost", 2727);
    }

    public void contactService(Whisper req) {

            try {
                translator = new Translator();
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                String packet = gson.toJson(req);
                //System.out.println("packet = "+ packet);
                dataOutputStream.writeUTF(packet);
                dataOutputStream.flush();
                System.out.println("Packet sent to service!");
                System.out.println("Waiting for response");
                String socketInput = dataInputStream.readUTF();
                Thread.sleep(30);
                System.out.println("socket input : " + socketInput);
                responseWhisper = translator.stringToWhisper(socketInput);
                //response = inputWhisper.getRequest();
//                if (response.equals("Error")){
//                    response = inputWhisper.getValueWithKey("Error Status");
//                }
//                else if(response.equals("Task Done")){
//                    response = "Successful!";
//                }
//                else{
//                    System.out.println("end");
//                }


            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

    }

    public static void setLoggedInUser(String username) throws IOException {

        File textFile = null;
        try {
            textFile = new File("./src/main/resources/loggedInUser.txt");
            if (textFile.createNewFile()) {
                System.out.println("File created: " + textFile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred creating file.");
            e.printStackTrace();
        }
        FileWriter writer = new FileWriter(textFile);
        writer.write(username);
        writer.close();
    }

    public static String getLoggedInUser() {

        String loggedInUser = null;
        try {
            File textFile = new File("./src/main/resources/loggedInUser.txt");
            Scanner reader = new Scanner(textFile);
            while (reader.hasNextLine()) {
                loggedInUser = reader.nextLine();
            }
            reader.close();
        }
        catch (FileNotFoundException e){
            System.out.println("Logged In User file not found");
        }
        return loggedInUser;

    }

    public static boolean createNewDirectory (String name)
    {
        String path = "./src/main/resources/Posts/";
        path += name;
        File file = new File(path);
        boolean isDone = file.mkdir();
        return isDone;
    }

    public Whisper getResponseWhisper() {
        return responseWhisper;
    }

    public void setResponseWhisper(Whisper responseWhisper) {
        this.responseWhisper = responseWhisper;
    }
}
