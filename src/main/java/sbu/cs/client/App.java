package sbu.cs.client;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import sbu.cs.Whisper;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception{
        Socket socket = new Socket("localhost", 2727);
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String request = "Sign Up";
        Map<String , String>  data = new HashMap<>();
        data.put("username", "3");
        data.put("password", "2");
        data.put("email", "2");
        Whisper req = new Whisper(request, data);
        /*
            Whisper packet = new Whisper();
            packet.createSignIn(username, password);
         */
        String packet = gson.toJson(req);
        dataOutputStream.writeUTF(packet);
        dataOutputStream.flush();
        System.out.println("Packet sent to service!");
        String income = dataInputStream.readUTF();
        Whisper response = gson.fromJson(income, Whisper.class);
        System.out.println(response);
        Scanner input = new Scanner(System.in);
        while (!input.nextLine().equals("end"))
        {

        }
        req = new Whisper();
        req.createEndTask();
        packet = gson.toJson(req);
        dataOutputStream.writeUTF(packet);
        dataOutputStream.flush();
        socket.close();
        dataInputStream.close();
        dataOutputStream.close();
    }
}
