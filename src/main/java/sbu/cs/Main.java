package sbu.cs;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sbu.cs.front.Controller.LogInController;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        TaskManager.createSocket();
        LogInController logInController = new LogInController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/LogIn.fxml"));
        loader.setController(logInController);
        Parent root= loader.load();
        primaryStage.setTitle("Instagram");
        primaryStage.getIcons().add(new Image("/Images/icon.png"));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }


}
