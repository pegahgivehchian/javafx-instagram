package sbu.cs.classes;

import javafx.scene.image.Image;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class PostObject {
    // Variables:
    private int ID;
    private int userID;
    private String username;
    private String pictureAddress;
    private String caption;
    private String postDate;
    private ArrayList<String> likers;
    private ArrayList<CommentObject> comments;


    // Functions:
    // Constructors:
    public PostObject(int id, int userID, String username, String pictureAddress, String caption, String postDate) //Normal constructor
    {
        likers = new ArrayList<>();
        comments = new ArrayList<>();
        this.ID = id;
        this.userID = userID;
        this.pictureAddress = pictureAddress;
        this.caption = caption;
        this.postDate = postDate;
        this.username = username;
    }

    public PostObject(String username, String pictureAddress, String caption) // Create a new post client
    {
        likers = new ArrayList<>();
        comments = new ArrayList<>();
        this.username = username;
        this.pictureAddress = pictureAddress;
        this.caption = caption;
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        this.postDate = LocalDateTime.now().format(dateFormat);
    }

    // Getters:

    public Image getImage(){
        //System.out.println("pic add" + pictureAddress);
        Image image = new Image(pictureAddress);
        return image;
    }

    public int getLikes(){
        return likers.size();
    }
    public int getID() {
        return ID;
    }

    public int getUserID() {
        return userID;
    }

    public String getPictureAddress() {
        return pictureAddress;
    }

    public String getCaption() {
        return caption;
    }

    public String getPostDate() {
        return postDate;
    }

    public String getUsername()
    {
        return username;
    }

    public ArrayList<String> getLikers()
    {
        return likers;
    }

    public ArrayList<CommentObject> getComments()
    {
        return comments;
    }

    // Setters:

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setPictureAddress(String pictureAddress) {
        this.pictureAddress = pictureAddress;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setLikers(ArrayList<String> likers)
    {
        this.likers = likers;
    }

    public void addLiker(String username)
    {
        this.likers.add(username);
    }

    public void setComments(ArrayList<CommentObject> comments)
    {
        this.comments = comments;
    }

    public void addComment(CommentObject comment)
    {
        this.comments.add(comment);
    }
}
