package sbu.cs.classes;

public class LikeObject {
    //Variables:
    private int postID;
    private int userID;



    private String username;
    //Functions:
    //Constructor:
    public LikeObject(int postID, int userID)
    {
        this.postID = postID;
        this.userID = userID;
    }

    public LikeObject(String username)
    {
        this.username = username;
    }

    //Getter:

    public int getPostID() {
        return postID;
    }

    public int getUserID() {
        return userID;
    }

    //Setters:


    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
