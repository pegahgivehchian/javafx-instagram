package sbu.cs.classes;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CommentObject {
    // Variables:
    private int ID;
    private int userID;
    private int postID;
    private String username;
    private String commentText;
    private String commentDate;



    // Functions:
    // Constructors:
    public CommentObject(int ID, int userID, int postID, String commentText, String commentDate) //Normal constructor
    {
        this.ID = ID;
        this.userID = userID;
        this.postID = postID;
        this.commentText = commentText;
        this.commentDate = commentDate;
    }

    public CommentObject(int userID, int postID, String commentText) // New comment constructor
    {
        this.ID = 0;
        this.userID = userID;
        this.postID = postID;
        this.commentText = commentText;
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        this.commentDate = LocalDateTime.now().format(dateFormat);
    }

    public CommentObject(String username, int postID, String commentText) // Client Constructor.
    {
        this.ID = 0;
        this.userID = 0;
        this.username = username;
        this.postID = postID;
        this.commentText = commentText;
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        this.commentDate = LocalDateTime.now().format(dateFormat);
    }

    public CommentObject(int ID, String username, int userID, int postID, String commentText, String commentDate)
    {
        this.ID = ID;
        this.username = username;
        this.commentDate = commentDate;
        this.commentText = commentText;
        this.userID = userID;
        this.postID = postID;
    }

    public CommentObject (String username , String commentText){
        this.username = username;
        this.commentText = commentText;
    }
    // Getters:


    public int getID() {
        return ID;
    }

    public int getUserID() {
        return userID;
    }

    public int getPostID() {
        return postID;
    }

    public String getCommentText() {
        return commentText;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public String getUsername(){ return username; }

    // Setters:


    public void setID(int ID) {
        this.ID = ID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
}
