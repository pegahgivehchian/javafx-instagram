package sbu.cs.classes;

public class UserDirectObject {

    private String username;

    public UserDirectObject(String username) {
        this.username = username;
    }

    public UserDirectObject() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
