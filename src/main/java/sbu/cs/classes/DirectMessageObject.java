package sbu.cs.classes;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DirectMessageObject {
    // Variables:
    private int ID;
    private String senderUsername;
    private String receiverUsername;
    private String messageText;
    private String messageDate;

    // Functions:
    public DirectMessageObject(int ID, String senderUsername, String receiverUsername, String messageText, String messageDate) // Normal constructor.
    {
        this.ID = ID;
        this.senderUsername = senderUsername;
        this.receiverUsername = receiverUsername;
        this.messageText = messageText;
        this.messageDate = messageDate;
    }

    public DirectMessageObject(String senderUsername, String receiverUsername, String messageText) //Client constructor.
    {
        this.senderUsername = senderUsername;
        this.receiverUsername = receiverUsername;
        this.messageText = messageText;
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        this.messageDate = LocalDateTime.now().format(dateFormat);
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public String getReceiverUsername() {
        return receiverUsername;
    }

    public void setReceiverUsername(String receiverUsername) {
        this.receiverUsername = receiverUsername;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }
}
