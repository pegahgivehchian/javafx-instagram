package sbu.cs.classes;

public class RelationshipObject {
    //Variables:
    private int followerID;
    private int followedID;

    //Functions:
    //Constructors:
    public RelationshipObject(int followerID, int followedID)
    {
        this.followedID = followedID;
        this.followerID = followerID;
    }

    //Getters:

    public int getFollowerID() {
        return followerID;
    }

    public int getFollowedID() {
        return followedID;
    }
    //Setters:

    public void setFollowerID(int followerID) {
        this.followerID = followerID;
    }

    public void setFollowedID(int followedID) {
        this.followedID = followedID;
    }
}
