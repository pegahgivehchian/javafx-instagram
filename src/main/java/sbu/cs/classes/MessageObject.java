package sbu.cs.classes;

public class MessageObject {

    private String message;

    public MessageObject(String message) {
        this.message = message;
    }

    public MessageObject() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
