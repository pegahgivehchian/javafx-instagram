package sbu.cs.classes;

public class UserObject {
    //Enums:
    public enum Sex
    {
        MALE, FEMALE, NOTBINARY
    }


    // Variables:
    private int ID;
    private String password;
    private String username;
    private String bio;
    private boolean isPrivate;
    private String profilePictureAddress;
    private String email;
    private Sex sex;

    //Functions:

    //Constructors:
    // Normal constructor for reprocess a signed up user.
    public UserObject(int id, String username, String password, String bio, boolean isPrivate, String profilePictureAddress, String email, Sex sex)
    {
        this.ID = id;
        this.username = username;
        this.password = password;
        this.bio = bio;
        this.isPrivate = isPrivate;
        this.profilePictureAddress = profilePictureAddress;
        this.email = email;
        this.sex = sex;
    }

    // Constructor for new user:
    public UserObject(String username, String password, String email)
    {
        this.ID = 0;
        this.username = username;
        this.password = password;
        this.email = email;
        this.bio = "";
        this.profilePictureAddress = "";
        this.sex = null;
        this.isPrivate = false;
    }

    // Getters:
    public int getID() {
        return ID;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getBio() {
        return bio;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public String getProfilePictureAddress() {
        return profilePictureAddress;
    }

    public String getEmail() {
        return email;
    }

    public Sex getSex() {
        return sex;
    }

    // Setters:

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public void setProfilePictureAddress(String profilePictureAddress) {
        this.profilePictureAddress = profilePictureAddress;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
